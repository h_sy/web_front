import axios from "axios";

const api = axios.create({
  baseURL: `${process.env.REACT_APP_API_URL}:${process.env.REACT_APP_PORT}/`,
});

export const userApi = {
  signUp: (username, email, password, nickname, job, location, snsBundle) =>
    api.post("auth/signup", {
      username,
      email,
      password,
      nickname,
      job,
      location,
      snsBundle,
    }),
  signIn: (username, password) =>
    api.post("auth/signin", { username, password }),
  tokenValidation: (accessToken) =>
    api.get("auth/validation", {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }),
  getProfile: (username) => api.get(`user/profile/${username}`),
  getMyProfile: (accessToken) =>
    api.get("user/profile/me", {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }),
  updateProfile: (formData, accessToken) =>
    api.put("user/profile", formData, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "multipart/form-data",
      },
    }),
  changePassword: (accessToken, password, newPassword) =>
    api.put(
      "user/password",
      { password, newPassword },
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    ),
  duplicationCheck: (username, nickname, email) =>
    api.get(
      `auth/duplcheck?username=${username}&nickname=${nickname}&email=${email}`
    ),
  follow: (userId = 1, accessToken = "") =>
    api.post(
      `user/follow/${userId}`,
      {},
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    ),
  unfollow: (userId = 1, accessToken = "") =>
    api.post(
      `user/unfollow/${userId}`,
      {},
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    ),
  checkIsFollowed: (userId = 1, accessToken = "") =>
    api.get(`user/isfollowed/${userId}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }),
  getFollowers: (userId = 1) => api.get(`user/followers/${userId}`),
  getFollowings: (userId = 1) => api.get(`user/followings/${userId}`),
  getFollowingCount: (userId = 1) => api.get(`user/followingcount/${userId}`),
  getFollowerCount: (userId = 1) => api.get(`user/followercount/${userId}`),
};

export const contentApi = {
  upload: (formData, accessToken) =>
    api.post("content/upload", formData, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "multipart/form-data",
      },
    }),
  getAll: ({ username = "", page = 0, countPerPage = 10 }) =>
    api.get(
      `content/all/${username}?page=${page}&countPerPage=${countPerPage}`
    ),
  getContent: (id, accessToken = "") =>
    api.get(`content/${id}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }),
  getMe: (accessToken) =>
    api.get("content/me", {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }),
  deleteContent: (accessToken, id) =>
    api.delete(`content/${id}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }),
  getRandom: ({ page = 0, countPerPage = 10, accessToken = "" }) =>
    api.get(`content/random?page=${page}&countPerPage=${countPerPage}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }),
  updateContent: ({ accessToken, id, title, description }) =>
    api.put(
      `content/${id}`,
      { title, description },
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    ),
  likeContent: (accessToken, contentId) =>
    api.post(
      `content/${contentId}/like`,
      {},
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    ),
  unlikeContent: (accessToken, contentId) =>
    api.post(
      `content/${contentId}/cancelLike`,
      {},
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    ),
  getBestContents: ({ accessToken = "" }) =>
    api.get("content/main", {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }),
  searchAll: ({ keyword, page = 0, countPerPage = 8 }) =>
    api.get(
      `content/search?input=${keyword}&page=${page}&countPerPage=${countPerPage}`
    ),
  searchJobs: ({ keyword, page = 0, countPerPage = 8 }) =>
    api.get(
      `content/search/job?input=${keyword}&page=${page}&countPerPage=${countPerPage}`
    ),
  searchContents: ({ keyword, page = 0, countPerPage = 8 }) =>
    api.get(
      `content/search/description?input=${keyword}&page=${page}&countPerPage=${countPerPage}`
    ),
  getAllLikes: ({ username = "", page = 0, countPerPage = 10 }) =>
    api.get(
      `content/mylikecontents/${username}?page=${page}&countPerPage=${countPerPage}`
    ),
};

export const replyApi = {
  sendReply: (accessToken, contentId, description) =>
    api.post(
      `reply/content/${contentId}`,
      { description },
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    ),
  getReplies: (contentId, page = 0, countPerPage = 10) =>
    api.get(
      `reply/content/${contentId}?page=${page}&countPerPage=${countPerPage}`
    ),
  sendReReply: (accessToken, replyId, description, toUserId = null) =>
    api.post(
      `reply/${replyId}/rereply`,
      { description, toUserId },
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    ),
  deleteReply: (accessToken, replyId) =>
    api.delete(`reply/${replyId}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }),
  deleteRereply: (accessToken, reReplyId) =>
    api.delete(`reply/rereply/${reReplyId}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }),
};

export const socialLoginApi = {
  getKakaoLogin: (accessToken) =>
    api.get("kakao/v2/user/me", {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }),
};
