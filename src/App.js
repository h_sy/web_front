import { BrowserRouter as Router } from "react-router-dom";
import GlobalStyles from "./components/globalStyles";
import { CookiesProvider } from "react-cookie";
import "./App.css";
import Main from "./components/main";
import Footer from "./components/footer";
import { HelmetProvider } from "react-helmet-async";

function App() {
  return (
    <div className="App">
      <HelmetProvider>
        <CookiesProvider>
          <Router>
            <Main />
          </Router>
          <Footer />
          <GlobalStyles />
        </CookiesProvider>
      </HelmetProvider>
    </div>
  );
}

export default App;
