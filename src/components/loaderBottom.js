import React from "react";
import styled from "styled-components";
import loadingGIF from "../img/Icons/loading.gif";

const Wrap = styled.div`
  width: 64px;
  height: 64px;
  margin: 30px auto;
`;

const Loading = styled.img`
  width: 100%;
`;

const LoaderBottom = () => {
  return (
    <Wrap>
      <Loading src={loadingGIF} />
    </Wrap>
  );
};

export default LoaderBottom;
