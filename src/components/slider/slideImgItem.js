import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const Container = styled.div`
  vertical-align: top;
`;

const Img = styled.img`
  width: 100%;
  min-height: 375px;
  vertical-align: top;
`;

const SlideImgItem = ({ img }) => {
  return (
    <Container>
      <Img src={img} />
    </Container>
  );
};

SlideImgItem.propTypes = {
  img: PropTypes.string.isRequired,
};

export default SlideImgItem;
