import React, { useRef, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Body1, Color } from "../mixIn";
import SearchSvg from "../../img/Icons/search_24px.svg";
import CancelSvg from "../../img/Icons/cancel.svg";

const Form = styled.form`
  display: none;
  position: relative;
  margin-right: 16px;

  @media screen and (min-width: 768px) {
    display: inline-block;
  }
`;

const SearchIcon = styled.img`
  width: 20px;
  height: 20px;
  position: absolute;
  top: 10px;
  left: 10px;
`;

const Input = styled.input`
  ${Body1};
  color: ${Color.DarkGrey};
  width: 328px;
  height: 40px;
  background: #ffffff;
  border: 1px solid #cccccc;
  box-sizing: border-box;
  border-radius: 4px;
  padding: 11px 36px;
  outline: 0;

  &:focus {
  }
`;

const CancelButton = styled.button`
  position: absolute;
  top: 0;
  right: 0;
  padding: 0;
  width: 36px;
  height: 40px;
  img {
    width: 18px;
    height: 18px;
  }
`;

function SearchBar({ history }) {
  const [isActive, setIsActive] = useState(false);
  const [keyword, setKeyword] = useState("");
  const inputRef = useRef();

  const handleSubmit = (e) => {
    e.preventDefault();
    if (keyword === "") return;

    setIsActive(false);
    inputRef.current.blur();
    setKeyword("");
    history.push(`/search?input=${keyword}&page=0&countPerPage=8`);
  };

  const handleCancel = (e) => {
    e.preventDefault();
    setIsActive(false);
    setKeyword("");
  };

  return (
    <Form onSubmit={(e) => handleSubmit(e)} onReset={(e) => handleCancel(e)}>
      <SearchIcon src={SearchSvg} />
      <Input
        type="text"
        placeholder="검색어를 입력하세요"
        ref={inputRef}
        value={keyword}
        onChange={(e) => setKeyword(e.target.value)}
        onFocus={() => setIsActive(true)}
        onBlur={() => {
          setTimeout(() => setIsActive(false), 100);
        }}
      />
      {isActive && (
        <CancelButton type="reset" onClick={(e) => handleCancel(e)}>
          <img src={CancelSvg} />
          <span className="blind">검색 취소</span>
        </CancelButton>
      )}
    </Form>
  );
}

SearchBar.propTypes = {};

export default SearchBar;
