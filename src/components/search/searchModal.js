import React, { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import searchIcon from "../../img/Icons/search_24px.svg";
import cancelIcon from "../../img/Icons/cancel.svg";

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  background-color: #fff;
  z-index: 100;
`;

const Wrap = styled.div`
  padding: 20px;
  background-color: #fff;

  @media screen and (min-width: 768px) {
    padding: 30px 0;
  }
`;

const Form = styled.form`
  position: relative;
  @media screen and (min-width: 768px) {
    width: 90%;
    max-width: 1060px;
    margin: 0 auto;
  }
`;

const Input = styled.input`
  height: 50px;
  line-height: 50px;
  width: 100%;
  padding: 0 36px 0 60px;
  border: 1px solid #f2f4f7;
  background-color: #f2f4f7;
  font-size: 16px;
  font-weight: 400;
  border-radius: 30px;
  outline: none;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  &:focus {
    border: 1px solid #77c4a3;
    color: #333;
    background-color: #fff;
  }
`;

const SearchImg = styled.img`
  position: absolute;
  width: 24px;
  height: 24px;
  top: 12px;
  left: 20px;
`;

const CancelButton = styled.button`
  position: absolute;
  top: 0;
  right: 0;
  width: 50px;
  height: 50px;
  img {
    width: 18px;
    height: 18px;
  }
`;

const Overlay = styled.div`
  content: "";
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.4);
  z-index: -1;
`;

function SearchModal({ isActive, setIsActive, history }) {
  const [keyword, setKeyword] = useState("");
  const inputRef = useRef();

  const handleSubmit = (e) => {
    e.preventDefault();
    if (keyword === "") {
      e.preventDefault();
      return;
    }

    setIsActive(false);
    setKeyword("");
    history.push(`/search?input=${keyword}&page=0&countPerPage=8`);
  };

  const handleCancel = (e) => {
    e.preventDefault();
    setIsActive(false);
    setKeyword("");
  };

  useEffect(() => {
    const pageClickEvent = (e) => {
      // If the active element exists and is clicked outside of
      if (inputRef.current !== null && !inputRef.current.contains(e.target)) {
        setIsActive(!isActive);
      }
    };

    // If the item is active (ie open) then listen for clicks
    if (isActive) {
      inputRef.current.focus();
      window.addEventListener("click", pageClickEvent);
    }

    return () => {
      window.removeEventListener("click", pageClickEvent);
    };
  }, [isActive]);

  return (
    <Container>
      <Wrap>
        <Form
          onSubmit={(e) => handleSubmit(e)}
          onReset={(e) => handleCancel(e)}
        >
          <Input
            type="text"
            ref={inputRef}
            placeholder="검색어를 입력하세요."
            value={keyword}
            onChange={(e) => setKeyword(e.target.value)}
          />
          <SearchImg src={searchIcon} />
          <CancelButton type="reset" onClick={(e) => handleCancel(e)}>
            <img src={cancelIcon} />
            <span className="blind">검색 취소</span>
          </CancelButton>
        </Form>
        <Overlay />
      </Wrap>
    </Container>
  );
}

SearchModal.propTypes = {
  isActive: PropTypes.bool.isRequired,
  setIsActive: PropTypes.func.isRequired,
};

export default SearchModal;
