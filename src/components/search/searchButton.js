import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import searchIcon from "../../img/Icons/search_24px.svg";

const Button = styled.button`
  padding: 0 10px;
  height: 40px;

  @media screen and (min-width: 768px) {
    display: none;
  }
`;

const Img = styled.img`
  width: 24px;
  height: 24px;

  @media screen and (min-width: 768px) {
    width: 28px;
    height: 28px;
  }
`;

function SearchButton({ setIsActive }) {
  return (
    <Button onClick={() => setIsActive((prev) => !prev)}>
      <Img src={searchIcon} />
      <span className="blind">검색</span>
    </Button>
  );
}

SearchButton.propTypes = {
  setIsActive: PropTypes.func.isRequired,
};

export default SearchButton;
