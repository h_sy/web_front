import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import basicProfile from "../img/Icons/btn_profile.svg";
import emptyHeart from "../img/Icons/empty heart.svg";
import fillHeart from "../img/Icons/heart.svg";
import commentIcon from "../img/Icons/comment.svg";
import noImage from "../img/no image.png";
import { Link } from "react-router-dom";
import { handleLikeContent, handleUnlikeContent } from "../util/httpUtil";
import { useConfirm } from "../hooks/useConfirm";
import { LoginContext } from "./main";
import {
  Body1,
  Caption,
  Subtitle2,
  Color,
  elip1,
  elip2,
  elip3,
  Subtitle1,
} from "./mixIn";

const SLink = styled(Link)`
  width: 100%;
`;

const ContainerWrap = styled.div`
  background-color: #ffffff;
  margin-top: 15px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  @media screen and (min-width: 768px) {
    flex: 0 1 50%;
    padding: 0 4px;

    &:nth-child(-n + 2) {
      margin-top: 0;
    }
  }

  @media screen and (min-width: 1024px) {
    flex: 0 1 33.3333%;

    &:nth-child(-n + 3) {
      margin-top: 0;
    }
  }

  @media screen and (min-width: 1200px) {
    flex: 0 1 25%;
    padding: 0 7px;

    &:nth-child(-n + 4) {
      margin-top: 0;
    }
  }
`;

const Container = styled.div`
  width: 100%;
  border: 1px solid rgba(0, 0, 0, 0.12);
  box-sizing: border-box;
  border-radius: 16px;
  overflow: hidden;
`;

const ImgWrap = styled.div`
  overflow: hidden;
  position: relative;
  padding-bottom: 100%;
`;

const Img = styled.img`
  width: 100%;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  object-fit: cover;
`;

const CardInfo = styled.div`
  padding: 16px;
`;

const Header = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 16px;
`;

const ProfileImgWrap = styled.div`
  width: 36px;
  height: 36px;
  border-radius: 100px;
  overflow: hidden;
  margin-right: 16px;
`;

const ProfileImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  vertical-align: top;
`;

const ProfileWrap = styled.div`
  width: calc(100% - 52px);
`;

const Nickname = styled.div`
  ${Subtitle2};
  color: ${Color.Grey3};
  ${elip1};
`;

const Occupation = styled.div`
  ${Caption};
  ${elip1};
  color: #666666;
`;

const Title = styled.h3`
  ${Subtitle1};
  color: ${Color.DarkGrey};
  ${elip1};
`;

const Description = styled.p`
  ${Body1};
  color: ${Color.Grey3};
  ${elip3};
  margin-top: 16px;
  height: 72px;

  @media screen and (min-width: 1200px) {
    ${elip2};
    height: 48px;
    margin-top: 8px;
  }
`;

const CountWrap = styled.div`
  margin-top: 24px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const CountTitle = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  &:not(:first-child) {
    margin-left: 24px;
  }
  span {
    font-size: 15px;
    line-height: 22px;
    color: ${Color.Grey3};
    margin-left: 10px;

    @media screen and (min-width: 1024px) {
      ${Body1};
    }
  }
`;

const Button = styled.button`
  width: 20px;
  height: 20px;
  padding: 0;

  img {
    width: 20px;
    height: 20px;
  }
`;

const Comment = styled.img`
  width: 20px;
  height: 20px;
`;

const Card = ({ cardRef, item, history }) => {
  const { accessToken, isLogin } = useContext(LoginContext);
  const [isLike, setIsLike] = useState(
    item.myLike ? (item.myLike === 1 ? true : false) : false
  );
  const [likeCount, setLikeCount] = useState(0);

  useEffect(() => {
    if (item && item.myLike === 1) {
      setIsLike(true);
    }

    if (item && item.contentExt) {
      setLikeCount(item.contentExt.like);
    }
  }, [item]);

  const needLogin = useConfirm(
    "로그인이 필요한 서비스입니다. 로그인 하시겠습니까?",
    () => history.push("/login"),
    () => {
      console.log("취소");
    }
  );

  const handleLike = (e) => {
    e.preventDefault();
    if (!isLogin) {
      return needLogin();
    }
    if (isLike) {
      handleUnlikeContent(accessToken, item.id);
      setLikeCount(likeCount - 1);
    } else {
      handleLikeContent(accessToken, item.id);
      setLikeCount(likeCount + 1);
    }
    setIsLike(!isLike);
  };

  return (
    <ContainerWrap ref={cardRef}>
      <Container>
        <SLink
          to={{
            pathname: `contents/${item.id}`,
            state: { itemId: item.id },
          }}
        >
          <Header>
            <ProfileImgWrap>
              <ProfileImg
                src={
                  item.user.profileImg === ""
                    ? basicProfile
                    : item.user.profileImg
                }
                alt=""
              />
            </ProfileImgWrap>
            <ProfileWrap>
              <Nickname>{item.user.nickname}</Nickname>
              <Occupation>{item.user.job}</Occupation>
            </ProfileWrap>
          </Header>
          <ImgWrap>
            <Img
              loading="lazy"
              src={item.images ? item.images[0] : noImage}
              alt={`${item.user.nickname}의 책상 사진`}
            />
          </ImgWrap>
          <CardInfo>
            <Title>{item.title}</Title>
            <Description>{item.description}</Description>
            <CountWrap>
              <CountTitle>
                <Button clicked={isLike} onClick={(e) => handleLike(e)}>
                  <img src={isLike ? fillHeart : emptyHeart} />
                  <span className="blind">좋아요</span>
                </Button>
                <span>{likeCount}</span>
              </CountTitle>
              <CountTitle>
                <Comment src={commentIcon} />
                <span className="blind">댓글</span>
                <span>{item.contentExt.replyCnt}</span>
              </CountTitle>
            </CountWrap>
          </CardInfo>
        </SLink>
      </Container>
    </ContainerWrap>
  );
};

Card.propTypes = {
  item: PropTypes.object.isRequired,
};

export default Card;
