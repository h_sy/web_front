import React from "react";
import styled from "styled-components";
import { Body3, Color } from "./mixIn";

const Container = styled.div`
  border-top: 1px solid ${Color.LightGrey2};
  text-align: center;
  padding: 30px 60px;
  margin-top: auto;
`;

const Copyright = styled.p`
  ${Body3};
  color: ${Color.Grey2};
`;

function Footer() {
  return (
    <Container>
      <Copyright>© Deskholic</Copyright>
    </Container>
  );
}

export default Footer;
