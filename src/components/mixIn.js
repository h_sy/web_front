import { css } from "styled-components";

export const Color = {
  DeskGreen: "#6cc5aa",
  DeskRed: "#ff5151",
  White: "#ffffff",
  LightGrey1: "#f5f5f5",
  LightGrey2: "#e0e0e0",
  Grey1: "#cccccc",
  Grey2: "#999999",
  Grey3: "#666666",
  DarkGrey: "#333333",
};

export const Headline1 = css`
  font-size: 32px;
  line-height: 48px;
  font-weight: bold;
`;

export const Headline2 = css`
  font-size: 24px;
  line-height: 36px;
  font-weight: bold;
`;

export const Subtitle1 = css`
  font-size: 18px;
  line-height: 28px;
  font-weight: bold;
`;

export const Subtitle2 = css`
  font-size: 16px;
  line-height: 24px;
  font-weight: bold;
`;

export const Body1 = css`
  font-size: 16px;
  line-height: 24px;
  font-weight: 400;
`;

export const Body2 = css`
  font-size: 14px;
  line-height: 22px;
  font-weight: bold;
`;

export const Body3 = css`
  font-size: 14px;
  line-height: 22px;
  font-weight: 400;
`;

export const Button1 = css`
  font-size: 16px;
  line-height: 24px;
  font-weight: bold;
`;

export const Button2 = css`
  font-size: 14px;
  line-height: 22px;
  font-weight: bold;
`;

export const Caption = css`
  font-size: 12px;
  line-height: 18px;
  font-weight: 400;
`;

export const elip1 = css`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  display: block;
`;

export const elip2 = css`
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
`;

export const elip3 = css`
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 3;
`;
