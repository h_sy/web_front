import React, { useEffect } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { FetchComment } from "../../util/httpUtil";
import CommentItem from "./commentItem";

const List = styled.ul``;

const CommentList = ({
  username,
  contentId,
  comments,
  setComments,
  setReplyCount,
  accessToken,
  profileImg,
  isLogin,
  history,
}) => {
  const { loading, data } = FetchComment(contentId);

  useEffect(() => {
    setComments(data);
  }, [data]);

  return (
    <List>
      {comments &&
        comments.map((item) => (
          <CommentItem
            key={item.id}
            item={item}
            username={username}
            accessToken={accessToken}
            profileImg={profileImg}
            isLogin={isLogin}
            history={history}
            comments={comments}
            setComments={setComments}
            setReplyCount={setReplyCount}
          />
        ))}
      {/* {loading && <div>loading...⏰</div>} */}
    </List>
  );
};

CommentList.propTypes = {
  username: PropTypes.string,
  contentId: PropTypes.number.isRequired,
  comments: PropTypes.array.isRequired,
  setComments: PropTypes.func.isRequired,
  setReplyCount: PropTypes.func.isRequired,
  accessToken: PropTypes.string,
  isLogin: PropTypes.bool,
};

export default CommentList;
