import React, { useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { replyApi } from "../../api";
import basicProfile from "../../img/Icons/btn_profile.svg";
import { Body1, Color } from "../mixIn";

const Container = styled.div`
  display: flex;
  padding: 10px;
`;

const Form = styled.form``;

const ImgWrap = styled.div`
  width: 36px;
  height: 36px;
  display: inline-block;
  vertical-align: middle;
  margin-right: 10px;
  border-radius: 100px;
  overflow: hidden;

  @media screen and (min-width: 768px) {
    margin-right: 17px;
  }
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  vertical-align: top;
`;

const ContentWrap = styled.div`
  position: relative;
  flex-grow: 1;
`;

const Textarea = styled.textarea`
  ${Body1};
  color: ${Color.DarkGrey};
  resize: none;
  width: 100%;
  height: 38px;
  padding: 7px 60px 7px 11px;
  border: 1px solid #cccccc;
  border-radius: 4px;
  box-sizing: border-box;
  vertical-align: top;

  &:focus {
    outline: none;
  }

  &::placeholder {
    color: ${Color.Grey1};
  }
`;

const Button = styled.button`
  position: absolute;
  top: 0;
  right: 0;
  width: 60px;
  height: 38px;
  padding: 0;
  box-sizing: border-box;
  color: ${Color.DeskGreen};
  font-size: 16px;
  line-height: 24px;
  font-weight: bold;
  opacity: ${(props) => (props.disabled ? 0.4 : 1)};
`;

const WritingComment = ({
  contentId,
  accessToken,
  comments,
  setComments,
  setReplyCount,
  profileImg,
}) => {
  const [description, setDescription] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (description === "") {
      return;
    }
    setDescription("");
    try {
      const response = await replyApi.sendReply(
        accessToken,
        contentId,
        description
      );

      if (response.status === 200) {
        if (response.data) {
          const mergeData = comments.concat(response.data);
          setComments(mergeData);
          setReplyCount((prev) => prev + 1);
        }
      }
    } catch (error) {
      console.log(error.response);
    }
  };
  return (
    <Form onSubmit={handleSubmit}>
      <Container>
        <ImgWrap>
          <Img src={profileImg === "" ? basicProfile : profileImg} />
        </ImgWrap>
        <ContentWrap>
          <Textarea
            value={description}
            onChange={(e) => setDescription(e.currentTarget.value)}
            placeholder="댓글 달기"
          />
          <Button disabled={description.length === 0} onClick={handleSubmit}>
            등록
          </Button>
        </ContentWrap>
      </Container>
    </Form>
  );
};

WritingComment.propTypes = {
  contentId: PropTypes.number.isRequired,
  accessToken: PropTypes.string.isRequired,
  comments: PropTypes.array.isRequired,
  setComments: PropTypes.func.isRequired,
  setReplyCount: PropTypes.func.isRequired,
  profileImg: PropTypes.string,
};

export default WritingComment;
