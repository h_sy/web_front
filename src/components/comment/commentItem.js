import React, { useEffect, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { useConfirm } from "../../hooks/useConfirm";
import { replyApi } from "../../api";
import moment from "moment";
import "moment/locale/ko";
import ReplyList from "./reply/replyList";
import WritingReply from "./reply/writingReply";
import basicProfile from "../../img/Icons/btn_profile.svg";
import { Body1, Body3, Subtitle2 } from "../mixIn";

const Container = styled.li`
  display: flex;
  flex-direction: column;
`;

const ProfileWrap = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  padding: 10px 0;

  @media screen and (min-width: 768px) {
    padding: 24px 0;
  }
`;

const ImgWrap = styled.div`
  width: 36px;
  height: 36px;
  display: inline-block;
  vertical-align: middle;
  margin: 0 10px;
  border-radius: 100px;
  overflow: hidden;

  @media screen and (min-width: 768px) {
    margin: 0 16px 0 10px;
  }
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  vertical-align: top;
`;

const ContentWrap = styled.div`
  margin-left: 56px;
  flex-grow: 1;
  padding-right: 10px;

  @media screen and (min-width: 768px) {
    margin-left: 62px;
  }
`;

const Nickname = styled.span`
  ${Subtitle2};
  color: rgba(0, 0, 0, 0.6);
  margin-right: 16px;
`;

const CommentContent = styled.span`
  ${Body1};
  color: rgba(0, 0, 0, 0.6);
  word-break: keep-all;
`;

const Footer = styled.div`
  margin-top: 4px;
`;

const DateBefore = styled.span`
  ${Body3};
  color: rgba(0, 0, 0, 0.4);
  margin-right: 16px;
`;

const Button = styled.button`
  padding: 0;
  font-size: 14px;
  line-height: 22px;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.4);
  margin-right: 16px;
  display: ${(props) => (props.isShow ? "inline-block" : "none")};
`;

const CommentItem = ({
  item,
  username,
  accessToken,
  profileImg,
  isLogin,
  history,
  comments,
  setComments,
  setReplyCount,
}) => {
  const [showWriting, setShowWriting] = useState(false);
  const [toUser, setToUser] = useState(null);
  const [replies, setReplies] = useState(
    item.replies === undefined ? [] : item.replies
  );
  const [writeDate, setWriteDate] = useState(null);

  useEffect(() => {
    if (item) {
      if (item.updatedAt === null) {
        const fromnow = moment(item.createdAt).fromNow();
        setWriteDate(fromnow);
      } else {
        const fromnow = moment(item.updatedAt).fromNow();
        setWriteDate(fromnow);
      }
    }
  }, [item]);

  const writeRereply = (user) => {
    if (!isLogin) {
      alert("로그인이 필요합니다.");
      history.push("/login");
    } else {
      setShowWriting(true);
      setToUser(user.id);
    }
  };

  const deleteReply = async () => {
    try {
      const response = await replyApi.deleteReply(accessToken, item.id);
      if (response.status === 200) {
        alert("삭제되었습니다.");
        setComments(comments.filter((comment) => comment.id !== item.id));
        setReplyCount((prev) => prev - (replies.length - 1));
      }
    } catch (error) {
      console.log(error.response);
    }
  };

  const confirmDeleteReply = useConfirm(
    "댓글을 삭제하시겠습니까?",
    deleteReply,
    () => console.log("취소")
  );

  return (
    <Container>
      <ProfileWrap>
        <ImgWrap>
          <Img
            src={
              item.user.profileImg === "" ? basicProfile : item.user.profileImg
            }
          />
        </ImgWrap>
        <div>
          <Nickname>{item.user.nickname}</Nickname>
          <CommentContent>{item.description}</CommentContent>
          <Footer>
            <DateBefore>{writeDate}</DateBefore>
            <Button isShow={true} onClick={() => writeRereply(item.user)}>
              답글달기
            </Button>
            <Button
              isShow={username === item.user.username}
              onClick={confirmDeleteReply}
            >
              삭제
            </Button>
          </Footer>
        </div>
      </ProfileWrap>
      <ContentWrap>
        <ReplyList
          accessToken={accessToken}
          username={username}
          writeRereply={writeRereply}
          items={replies}
          setReplies={setReplies}
          setReplyCount={setReplyCount}
        />
        <WritingReply
          show={showWriting}
          setShowWriting={setShowWriting}
          accessToken={accessToken}
          profileImg={profileImg}
          replyId={item.id}
          toUser={toUser}
          replies={replies}
          setReplies={setReplies}
          setReplyCount={setReplyCount}
        />
      </ContentWrap>
    </Container>
  );
};

CommentItem.propTypes = {
  item: PropTypes.object.isRequired,
  username: PropTypes.string,
  accessToken: PropTypes.string,
  isLogin: PropTypes.bool,
  comments: PropTypes.array.isRequired,
  setComments: PropTypes.func.isRequired,
  setReplyCount: PropTypes.func.isRequired,
};

export default CommentItem;
