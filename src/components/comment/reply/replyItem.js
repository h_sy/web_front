import React, { useEffect, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import basicProfile from "../../../img/Icons/btn_profile.svg";
import moment from "moment";
import "moment/locale/ko";
import { useConfirm } from "../../../hooks/useConfirm";
import { replyApi } from "../../../api";
import { Color, Body1, Body3, Subtitle2 } from "../../mixIn";

const Container = styled.li`
  padding: 10px;
  display: flex;
  background: rgba(108, 197, 170, 0.08);
  border-radius: 4px;

  @media screen and (min-width: 768px) {
    padding: 24px;
  }
`;

const ImgWrap = styled.div`
  width: 36px;
  height: 36px;
  display: inline-block;
  vertical-align: middle;
  border-radius: 100px;
  overflow: hidden;
  margin-right: 10px;

  @media screen and (min-width: 768px) {
    margin: 0 16px 0 10px;
  }
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  vertical-align: top;
`;

const ContentWrap = styled.div`
  display: inline-block;
  vertical-align: middle;
  flex-grow: 1;
  max-width: 83%;
`;

const Nickname = styled.span`
  font-size: 15px;
  line-height: 22px;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.6);
  margin-right: 10px;

  @media screen and (min-width: 768px) {
    ${Subtitle2};
    margin-right: 16px;
  }
`;

const CommentContent = styled.span`
  font-size: 15px;
  line-height: 22px;
  color: rgba(0, 0, 0, 0.6);
  word-break: break-word;

  @media screen and (min-width: 768px) {
    ${Body1};
  }
`;

const Footer = styled.div`
  color: #757575;
  font-size: 13px;
  margin-top: 5px;
`;

const DateBefore = styled.span`
  ${Body3};
  color: rgba(0, 0, 0, 0.4);
  margin-right: 16px;
`;

const ReplyButton = styled.button`
  padding: 0;
  font-size: 14px;
  line-height: 22px;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.4);
  margin-right: 16px;
`;

const Tagging = styled.span`
  color: ${Color.DeskGreen};
  font-size: 15px;
  line-height: 22px;
  font-weight: bold;
  margin-right: 8px;

  @media screen and (min-width: 768px) {
    ${Subtitle2};
    margin-right: 10px;
  }
`;

const Button = styled.button`
  padding: 0;
  font-size: 14px;
  line-height: 22px;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.4);
  display: ${(props) => (props.isShow ? "inline-block" : "none")};
`;

const ReplyItem = ({
  accessToken,
  username,
  writeRereply,
  replies,
  item,
  setReplies,
  setReplyCount,
}) => {
  const [writeDate, setWriteDate] = useState(null);

  useEffect(() => {
    if (item) {
      if (item.updatedAt === null) {
        const fromnow = moment(item.createdAt).fromNow();
        setWriteDate(fromnow);
      } else {
        const fromnow = moment(item.updatedAt).fromNow();
        setWriteDate(fromnow);
      }
    }
  }, [item]);

  const deleteRereply = async () => {
    try {
      const response = await replyApi.deleteRereply(accessToken, item.id);
      if (response.status === 200) {
        alert("삭제되었습니다.");
        setReplies(replies.filter((reply) => reply.id !== item.id));
        setReplyCount((prev) => prev - 1);
      }
    } catch (error) {
      console.log(error, error.response);
    }
  };

  const confirmDeleteReply = useConfirm(
    "댓글을 삭제하시겠습니까?",
    deleteRereply,
    () => console.log("취소")
  );

  return (
    <Container>
      <ImgWrap>
        <Img
          src={
            item.user.profileImg === "" ? basicProfile : item.user.profileImg
          }
        />
      </ImgWrap>
      <ContentWrap>
        <Nickname>{item.user.nickname}</Nickname>
        <CommentContent>
          {item.toUser !== null && (
            <Tagging>{`@${item.toUser.nickname}`}</Tagging>
          )}
          {item.description}
        </CommentContent>
        <Footer>
          <DateBefore>{writeDate}</DateBefore>
          <ReplyButton onClick={() => writeRereply(item.user)}>
            답글달기
          </ReplyButton>
          <Button
            isShow={username === item.user.username}
            onClick={confirmDeleteReply}
          >
            삭제
          </Button>
        </Footer>
      </ContentWrap>
    </Container>
  );
};

ReplyItem.propTypes = {
  accessToken: PropTypes.string,
  username: PropTypes.string,
  writeRereply: PropTypes.func.isRequired,
  replies: PropTypes.array.isRequired,
  item: PropTypes.object.isRequired,
  setReplies: PropTypes.func.isRequired,
  setReplyCount: PropTypes.func.isRequired,
};

export default ReplyItem;
