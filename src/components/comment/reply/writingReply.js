import React, { useRef, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { replyApi } from "../../../api";
import basicProfile from "../../../img/Icons/btn_profile.svg";
import { Body1, Color } from "../../mixIn";

const Container = styled.div`
  display: flex;
  padding: 10px 0 10px 10px;

  @media screen and (min-width: 768px) {
    padding: 10px 0;
  }
`;

const Form = styled.form`
  display: ${(props) => (props.show ? "block" : "none")};
`;

const ImgWrap = styled.div`
  width: 36px;
  height: 36px;
  display: inline-block;
  vertical-align: middle;
  margin-right: 10px;
  border-radius: 100px;
  overflow: hidden;
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  vertical-align: top;
`;

const ContentWrap = styled.div`
  position: relative;
  flex-grow: 1;
  flex-basis: 255px;
`;

const Textarea = styled.textarea`
  ${Body1};
  color: ${Color.DarkGrey};
  resize: none;
  width: 100%;
  height: 38px;
  padding: 7px 60px 7px 11px;
  border: 1px solid #cccccc;
  border-radius: 4px;
  box-sizing: border-box;
  vertical-align: top;

  &:focus {
    outline: none;
  }

  &::placeholder {
    color: ${Color.Grey1};
  }
`;

const Button = styled.button`
  position: absolute;
  top: 0;
  right: 0;
  width: 60px;
  height: 38px;
  padding: 0;
  box-sizing: border-box;
  color: ${Color.DeskGreen};
  font-size: 16px;
  line-height: 24px;
  font-weight: bold;
  opacity: ${(props) => (props.disabled ? 0.4 : 1)};
`;

const WritingReply = ({
  show,
  setShowWriting,
  accessToken,
  profileImg,
  replyId,
  toUser,
  replies,
  setReplies,
  setReplyCount,
}) => {
  const [description, setDescription] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (description === "") {
      return;
    }

    try {
      const response = await replyApi.sendReReply(
        accessToken,
        replyId,
        description,
        toUser
      );
      if (response.status === 200) {
        if (response.data) {
          const mergeData = replies.concat(response.data);
          setReplies(mergeData);
          setReplyCount((prev) => prev + 1);
        }
        setDescription("");
        setShowWriting(false);
      }
    } catch (error) {
      console.log(error.response);
    }
  };

  return (
    <Form show={show} onSubmit={handleSubmit}>
      <Container>
        <ImgWrap>
          <Img src={profileImg === "" ? basicProfile : profileImg} />
        </ImgWrap>
        <ContentWrap>
          <Textarea
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            placeholder="댓글 달기"
          />
          <Button disabled={description.length === 0} onClick={handleSubmit}>
            등록
          </Button>
        </ContentWrap>
      </Container>
    </Form>
  );
};

WritingReply.propTypes = {
  show: PropTypes.bool.isRequired,
  setShowWriting: PropTypes.func.isRequired,
  accessToken: PropTypes.string,
  replyId: PropTypes.number.isRequired,
  toUser: PropTypes.number,
  replies: PropTypes.array.isRequired,
  setReplies: PropTypes.func.isRequired,
  setReplyCount: PropTypes.func.isRequired,
};

export default WritingReply;
