import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import ReplyItem from "./replyItem";

const List = styled.ul``;

const ReplyList = ({
  accessToken,
  username,
  writeRereply,
  items,
  setReplies,
  setReplyCount,
}) => {
  return (
    <List>
      {items &&
        items.length > 0 &&
        items.map((item) => (
          <ReplyItem
            key={item.id}
            accessToken={accessToken}
            username={username}
            writeRereply={writeRereply}
            replies={items}
            item={item}
            setReplies={setReplies}
            setReplyCount={setReplyCount}
          />
        ))}
    </List>
  );
};

ReplyList.propTypes = {
  accessToken: PropTypes.string,
  username: PropTypes.string,
  writeRereply: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired,
  setReplies: PropTypes.func.isRequired,
  setReplyCount: PropTypes.func.isRequired,
};

export default ReplyList;
