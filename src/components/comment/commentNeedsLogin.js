import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const Container = styled.div`
  padding: 15px 10px;
  text-align: center;
  font-size: 15px;
  color: #757575;

  @media screen and (min-width: 768px) {
    padding: 10px 10px 30px;
  }
`;

const SLink = styled(Link)`
  font-size: 15px;
  color: #77c4a3;

  &:hover {
    border-bottom: 1px solid #77c4a3;
  }
`;

const CommentNeedsLogin = () => {
  return (
    <Container>
      댓글을 작성하려면 <SLink to="/login">로그인</SLink>이 필요합니다.
    </Container>
  );
};

export default CommentNeedsLogin;
