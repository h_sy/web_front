import React from "react";
import { Helmet } from "react-helmet-async";

function HelmetForSEO({ title, description, image, url }) {
  return (
    <Helmet>
      <title>{title}</title>
      <meta property="og:title" content={title} data-react-helmet="true" />
      {url !== "" && url !== null && url !== undefined && (
        <meta
          property="og:url"
          content={`https://web.deskholic.com/${url}`}
          data-react-helmet="true"
        />
      )}
      {description !== "" && description !== null && description !== undefined && (
        <>
          <meta
            name="description"
            content={description}
            data-react-helmet="true"
          />
          <meta
            property="og:description"
            content={description}
            data-react-helmet="true"
          />
        </>
      )}

      {image !== "" && image !== null && image !== undefined && (
        <meta property="og:image" content={image} data-react-helmet="true" />
      )}
    </Helmet>
  );
}

export default HelmetForSEO;
