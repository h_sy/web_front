import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import basicProfile from "../img/Icons/btn_profile.svg";
import { Link } from "react-router-dom";
import { userApi } from "../api";
import FollowButton from "./follow/followButton";
import { MypageContext } from "../pages/myPage/myPage";
import {
  Body2,
  Body3,
  Button1,
  Button2,
  Caption,
  Color,
  elip1,
  Headline2,
  Subtitle1,
} from "./mixIn";

const Container = styled.div`
  padding: 20px 15px;
  box-sizing: border-box;
  border-bottom: 1px solid ${Color.LightGrey2};
  position: relative;

  @media screen and (min-width: 768px) {
    padding: 40px;
  }

  @media screen and (min-width: 1024px) {
    max-width: 960px;
    margin: 60px 0 0;
    padding: 32px;
    border: 1px solid ${Color.LightGrey2};
    border-radius: 16px;
  }
`;

const ProfileWrap = styled.div`
  display: flex;
  flex-direction: row;
`;

const ImgWrap = styled.div`
  width: 80px;
  height: 80px;
  border-radius: 100px;
  display: inline-block;
  vertical-align: middle;
  overflow: hidden;
  margin-right: 16px;

  @media screen and (min-width: 768px) {
    width: 100px;
    height: 100px;
    margin-right: 24px;
  }

  @media screen and (min-width: 1024px) {
    width: 120px;
    height: 120px;
    margin-right: 32px;
  }
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const InfoWrap = styled.div`
  display: inline-block;
  font-size: 14px;
  vertical-align: middle;
  width: calc(100% - 115px);

  @media screen and (min-width: 768px) {
    width: calc(100% - 250px);
  }

  @media screen and (min-width: 1024px) {
    width: calc(100% - 400px);
  }
`;

const Nickname = styled.div`
  ${Subtitle1};
  color: ${Color.DarkGrey};
  ${elip1};

  @media screen and (min-width: 768px) {
    ${Headline2};
  }
`;

const Occupation = styled.div`
  ${Caption};
  color: ${Color.Grey2};
  ${elip1};

  @media screen and (min-width: 768px) {
    ${Body3};
  }
`;

const Follow = styled.div`
  margin-top: 10px;

  @media screen and (min-width: 768px) {
    margin-top: 24px;
  }
`;

const FollowWrap = styled.div`
  display: inline-block;

  &:not(:first-child) {
    margin-left: 24px;
  }
`;

const SLink = styled(Link)``;

const FollowTitle = styled.span`
  ${Body3};
  color: ${Color.Grey2};
  margin-right: 4px;
`;

const FollowNum = styled.span`
  ${Body2};
  color: ${Color.DeskGreen};
`;

const Introduction = styled.p`
  padding: 15px 0;
  font-size: 13px;
  min-height: 80px;
  max-height: 152px;
  box-sizing: border-box;

  @media screen and (min-width: 1024px) {
    min-height: 0;
    margin-left: 152px;
    padding: 8px 0;
    font-size: 14px;
  }
`;

const ButtonWrap = styled.div`
  position: absolute;
  top: 68px;
  right: 15px;

  @media screen and (min-width: 768px) {
    top: 40px;
    right: 40px;
  }

  @media screen and (min-width: 1024px) {
    top: 72px;
    right: 32px;
  }
`;

const SettingButton = styled.button`
  ${Button2};
  background-color: ${Color.White};
  color: ${Color.DarkGrey};
  padding: 4px 12px;
  width: 80px;
  height: 40px;
  border-radius: 4px;
  z-index: 10;
  box-sizing: border-box;
  border: 1px solid ${Color.LightGrey2};

  @media screen and (min-width: 768px) {
    ${Button1};
    width: 112px;
    padding: 8px 22px;
  }

  @media screen and (min-width: 1024px) {
    width: 160px;
  }
`;

const FollowButtonWrap = styled.div`
  @media screen and (min-width: 1024px) {
    width: 160px;
  }
`;

const ProfileCard = ({ userProfile, history }) => {
  const { isMypage } = useContext(MypageContext);
  const { id, profileImg, nickname, job, introduction } = userProfile;
  const [followerCount, setFollowerCount] = useState(0);
  const [followingCount, setFollowingCount] = useState(0);

  useEffect(() => {
    const getFollowerCount = async (userId) => {
      try {
        const { status, data } = await userApi.getFollowerCount(userId);
        if (status === 200) {
          setFollowerCount(data.followerCount);
        }
      } catch (error) {
        console.log(error.response);
      }
    };

    const getFollowingCount = async (userId) => {
      try {
        const { status, data } = await userApi.getFollowingCount(userId);
        if (status === 200) {
          setFollowingCount(data.followingCount);
        }
      } catch (error) {
        console.log(error.response);
      }
    };

    if (id !== undefined || id !== "") {
      getFollowerCount(id);
      getFollowingCount(id);
    }
  }, [id]);

  return (
    <Container>
      <ProfileWrap>
        <ImgWrap>
          <Img src={profileImg === "" ? basicProfile : profileImg} />
        </ImgWrap>
        <InfoWrap>
          <Nickname>{nickname}</Nickname>
          <Occupation>{job}</Occupation>
          <Follow>
            <FollowWrap>
              <SLink to={`/users/${id}/follower`}>
                <FollowTitle>팔로워</FollowTitle>
                <FollowNum>{followerCount}</FollowNum>
              </SLink>
            </FollowWrap>
            <FollowWrap>
              <SLink to={`/users/${id}/followee`}>
                <FollowTitle>팔로잉</FollowTitle>
                <FollowNum>{followingCount}</FollowNum>
              </SLink>
            </FollowWrap>
          </Follow>
        </InfoWrap>
      </ProfileWrap>

      <Introduction>{introduction}</Introduction>
      <ButtonWrap>
        {isMypage ? (
          <SLink
            to={{
              pathname: `/users/${id}/setting`,
              state: { userProfile },
            }}
          >
            <SettingButton>설정</SettingButton>
          </SLink>
        ) : (
          <FollowButtonWrap>
            {id && <FollowButton userId={id} history={history} />}
          </FollowButtonWrap>
        )}
      </ButtonWrap>
    </Container>
  );
};

ProfileCard.propTypes = {
  userProfile: PropTypes.object.isRequired,
};

export default ProfileCard;
