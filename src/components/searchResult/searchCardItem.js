import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import sampleDesk from "../../img/sample_desk.jpeg";
import sampleDesk2 from "../../img/sample_desk2.jpeg";
import { Link } from "react-router-dom";

const Container = styled.div`
  flex: 0 0 50%;
  max-width: 50%;
  padding: 0 5px;

  @media screen and (min-width: 768px) {
    padding: 0 10px;
    flex: 0 0 33.333%;
  }

  @media screen and (min-width: 1024px) {
    padding: 0 12px;
    flex: 0 0 25%;
  }
`;

const Article = styled.article`
  padding-bottom: 100%;
  margin-bottom: 10px;
  position: relative;

  @media screen and (min-width: 768px) {
    margin-bottom: 20px;
  }
`;

const ImgWrap = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border-radius: 5px;
  overflow: hidden;
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  vertical-align: top;
`;

const SLink = styled(Link)`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
  cursor: pointer;
`;

const User = styled.div`
  position: absolute;
  left: 8px;
  bottom: 8px;
  color: #fff;
  font-weight: 500;
`;

const Nickname = styled.div`
  font-size: 14px;
  line-height: 18px;
`;

const LikeCount = styled.div`
  font-size: 12px;
  line-height: 14px;
`;

function SearchCardItem({ itemRef, item }) {
  return item ? (
    <Container ref={itemRef}>
      <Article>
        <SLink
          to={{
            pathname: `/contents/${item.id}`,
            state: { itemId: item.id },
          }}
        />
        <ImgWrap>
          <Img src={item.images[0]} />
        </ImgWrap>
        <User>
          <LikeCount>❤︎ {item.contentExt.like}</LikeCount>
          <Nickname>{item.user.nickname}</Nickname>
        </User>
      </Article>
    </Container>
  ) : (
    <div>nothing</div>
  );
}

SearchCardItem.propTypes = {
  item: PropTypes.object.isRequired,
};

export default SearchCardItem;
