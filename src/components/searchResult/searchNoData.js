import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import noResultIcon from "../../img/Icons/desk.svg";
import { Color, Body1 } from "../mixIn";

const Container = styled.div`
  text-align: center;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 100%;
`;

const Text = styled.p`
  ${Body1};
  color: ${Color.Grey3};
  margin-top: 30px;
`;

const Img = styled.img`
  width: 180px;
  height: 103px;
`;

function SearchNoData() {
  return (
    <Container>
      <Img src={noResultIcon} />
      <Text>찾으시는 검색 결과가 없습니다.</Text>
    </Container>
  );
}

SearchNoData.propTypes = {};

export default SearchNoData;
