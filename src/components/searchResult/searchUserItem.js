import React, { useContext } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import basicProfile from "../../img/Icons/btn_profile.svg";
import { Link } from "react-router-dom";
import FollowButton from "../follow/followButton";
import { LoginContext, MyprofileContext } from "../main";
import { Body3, Color, Subtitle1, elip1 } from "../mixIn";

const SLink = styled(Link)``;

const Container = styled.div`
  display: flex;
  padding: 15px;
  align-items: center;
  border: 1px solid ${Color.LightGrey2};
  box-sizing: border-box;
  border-radius: 16px;

  @media screen and (min-width: 768px) {
    padding: 20px;
    width: 60%;
  }

  @media screen and (min-width: 1024px) {
    width: 50%;
  }
`;

const ProfileImgWrap = styled.div`
  width: 40px;
  height: 40px;
  margin-right: 10px;
  border-radius: 100px;
  overflow: hidden;

  @media screen and (min-width: 768px) {
    width: 58px;
    height: 58px;
    margin-right: 16px;
  }
`;

const ProfileImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const UserInfoWrap = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: calc(100% - 130px);
  height: 40px;
  @media screen and (min-width: 768px) {
    width: calc(100% - 186px);
  }
`;

const Nickname = styled.h3`
  ${elip1};
  color: ${Color.DarkGrey};
  margin: 0;
  font-size: 14px;
  font-weight: 500;
  line-height: 20px;

  @media screen and (min-width: 768px) {
    ${Subtitle1};
  }
`;

const Job = styled.h4`
  margin: 0;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  color: ${Color.Grey2};
  font-size: 11px;
  font-weight: 400;
  line-height: 17px;

  @media screen and (min-width: 768px) {
    ${Body3};
  }
`;

const FollowButtonWrap = styled.div`
  @media screen and (min-width: 768px) {
    width: 112px;
  }
`;

function SearchUserItem({ user, history }) {
  const { isLogin } = useContext(LoginContext);
  const { myProfile } = useContext(MyprofileContext);

  return (
    <SLink to={`/users/${user.username}`}>
      <Container>
        <ProfileImgWrap>
          <ProfileImg src={user.profileImg ? user.profileImg : basicProfile} />
        </ProfileImgWrap>
        <UserInfoWrap>
          <Nickname>{user.nickname}</Nickname>
          <Job>{user.job}</Job>
        </UserInfoWrap>
        {(!isLogin || (isLogin && myProfile && myProfile.id !== user.id)) && (
          <FollowButtonWrap>
            <FollowButton userId={user.id} history={history} />
          </FollowButtonWrap>
        )}
      </Container>
    </SLink>
  );
}

SearchUserItem.propTypes = {
  user: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default SearchUserItem;
