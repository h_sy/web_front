import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Color } from "../mixIn";

const Img = styled.img`
  width: 80px;
  height: 80px;
  border-radius: 16px;
  border: 1px solid ${Color.LightGrey2};
  box-sizing: border-box;
  vertical-align: top;

  @media screen and (min-width: 768px) {
    width: 120px;
    height: 120px;
  }

  &:not(:first-child) {
    margin-left: 5px;

    @media screen and (min-width: 768px) {
      margin-left: 10px;
    }
  }
`;

const UploadedItem = ({ image }) => {
  return <Img src={image} />;
};

UploadedItem.propTypes = {
  image: PropTypes.string.isRequired,
};

export default UploadedItem;
