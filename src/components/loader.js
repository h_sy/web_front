import React from "react";
import styled from "styled-components";
import loadingGIF from "../img/Icons/loading.gif";

const LoadingOuterWrap = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1000;
`;

const LoadingInnerWrap = styled.div`
  position: relative;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  box-sizing: border-box;
  width: 64px;
  height: 64px;
`;

const Loading = styled.img`
  width: 100%;
`;

const Loader = () => {
  return (
    <LoadingOuterWrap>
      <LoadingInnerWrap>
        <Loading src={loadingGIF} />
      </LoadingInnerWrap>
    </LoadingOuterWrap>
  );
};

export default Loader;
