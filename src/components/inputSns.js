import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import minusIcon from "../img/Icons/cancel.svg";
import { Body1, Color } from "./mixIn";

const Container = styled.div`
  position: relative;
  width: 100%;

  &:not(:first-child) {
    margin-top: 5px;
  }
`;

const Input = styled.input`
  ${Body1};
  color: ${Color.DarkGrey};
  width: 100%;
  padding: 7px 32px 7px 11px;
  border: 1px solid ${Color.Grey1};
  box-sizing: border-box;
  border-radius: 4px;
  margin-left: -1px;
  outline: none;

  &::placeholder {
    color: ${Color.Grey1};
  }
`;

const Button = styled.button`
  position: absolute;
  top: 0;
  right: 2px;
  padding: 20px 16px;
  background: url(${minusIcon}) no-repeat 8px 12px/ 15px 15px;
`;

const InputSns = ({ sns, updateSns, removeSnsClick }) => {
  return (
    <Container>
      <Input type="text" value={sns} placeholder="SNS" onChange={updateSns} />
      <Button onClick={removeSnsClick} />
    </Container>
  );
};

InputSns.propTypes = {
  sns: PropTypes.string.isRequired,
  updateSns: PropTypes.func.isRequired,
  removeSnsClick: PropTypes.func.isRequired,
};

export default InputSns;
