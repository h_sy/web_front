import React, { useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import ViewMyLikes from "../pages/myPage/myProfile/viewMyLikes";
import ViewMyUploads from "../pages/myPage/myProfile/viewMyUploads";
import { Color, Button1, Button2 } from "./mixIn";

const Container = styled.div`
  width: 100%;
  background-color: #ffffff;

  @media screen and (min-width: 768px) {
    max-width: 960px;
    margin: 0 auto;
    margin-top: 20px;
  }

  @media screen and (min-width: 1024px) {
    margin-top: 64px;
  }
`;

const List = styled.ul`
  border-bottom: 1px solid #e8e8e8;
  display: flex;
`;

const Item = styled.li`
  width: 50%;
  text-align: center;

  span {
    ${Button2};
    color: ${(props) =>
      props.active ? `${Color.DeskGreen}` : `${Color.Grey2}`};
    display: inline-block;
    padding: 10px 28px;
    border-bottom: ${(props) =>
      props.active ? `4px solid ${Color.DeskGreen};` : 0};
  }

  @media screen and (min-width: 768px) {
    span {
      ${Button1};
      padding: 14px 32px;
    }
  }
`;

const MyProfileTab = () => {
  const [activeTab, setActiveTab] = useState("myUpload");
  const obj = {
    myUpload: <ViewMyUploads />,
    myLikes: <ViewMyLikes />,
  };

  const handleClick = (tabId) => {
    setActiveTab(tabId);
  };

  return (
    <Container>
      <List>
        <Item
          active={activeTab === "myUpload"}
          onClick={() => handleClick("myUpload")}
        >
          <span>업로드</span>
        </Item>
        <Item
          active={activeTab === "myLikes"}
          onClick={() => handleClick("myLikes")}
        >
          <span>좋아요</span>
        </Item>
      </List>
      {obj[activeTab]}
    </Container>
  );
};

MyProfileTab.propTypes = {};

export default MyProfileTab;
