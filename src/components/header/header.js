import React, { useContext, useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import styled from "styled-components";
import PropTypes from "prop-types";
import logo from "../../img/Icons/logo.svg";
import ProfileAfterLogin from "./profileAfterLogin";
import SearchButton from "../search/searchButton";
import SearchModal from "../search/searchModal";
import { LoginContext, MyprofileContext } from "../main";
import SearchBar from "../search/searchBar";
import { Button1, Color } from "../mixIn";

const Container = styled.div`
  border-bottom: 1px solid ${Color.LightGrey2};
`;

const Wrap = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 15px;
  max-width: 1320px;
  margin: 0 auto;

  @media screen and (min-width: 768px) {
    padding: 0 34px;
  }
`;

const Logo = styled.div`
  width: 60px;
  height: 60px;
  background: url(${logo}) no-repeat 12px -6px/169px 70px;

  @media screen and (min-width: 768px) {
    width: 169px;
    height: 70px;
    background: url(${logo}) no-repeat 0 0/169px 70px;
  }
`;

const Button = styled.button`
  ${Button1};
  color: ${Color.White};
  background-color: ${Color.DeskGreen};
  width: 100px;
  height: 40px;
  border-radius: 4px;
`;

const SLink = styled(Link)`
  display: inline-block;
  vertical-align: top;
`;

const ItemWrap = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const Header = () => {
  const [isActive, setIsActive] = useState(false);
  const history = useHistory();
  const { isLogin } = useContext(LoginContext);
  const { myProfile } = useContext(MyprofileContext);

  return (
    <Container>
      <Wrap>
        <SLink to="/">
          <Logo>
            <span className="blind">Desk Holic</span>
          </Logo>
        </SLink>
        <ItemWrap>
          <SearchBar history={history} />
          <SearchButton setIsActive={setIsActive} />
          {isLogin ? (
            <ProfileAfterLogin
              username={myProfile.username}
              profileImg={myProfile.profileImg}
            />
          ) : (
            <SLink to="/login">
              <Button>LOGIN</Button>
            </SLink>
          )}
        </ItemWrap>
      </Wrap>
      {isActive && (
        <SearchModal
          isActive={isActive}
          setIsActive={setIsActive}
          history={history}
        />
      )}
    </Container>
  );
};

Header.propTypes = {};

export default Header;
