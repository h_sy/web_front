import React, { useRef } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import PropTypes from "prop-types";
import basicProfile from "../../img/Icons/btn_profile.svg";
import { useCookies } from "react-cookie";
import { useDetectOutsideClick } from "../../hooks/useDetectOutsideClick";
import { Color, Body1 } from "../mixIn";

const Container = styled.div`
  position: relative;
  display: inline-block;
  vertical-align: top;
`;

const ProfileButton = styled.button`
  padding: 0 10px;
`;

const ImgWrap = styled.div`
  display: inline-block;
  width: 30px;
  height: 30px;
  vertical-align: middle;
  border-radius: 100px;
  border: 0;
  overflow: hidden;
  box-sizing: border-box;

  @media screen and (min-width: 768px) {
    width: 40px;
    height: 40px;
  }
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  vertical-align: top;
`;

const SLink = styled(Link)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  display: block;
`;

const LogoutButton = styled.button`
  ${Body1};
  color: ${Color.DarkGrey};
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  display: block;
  text-align: left;
  padding: 9px 8px;
  margin: 8px 0;

  &:hover {
    font-weight: bold;
  }
`;

const Dropdown = styled.div`
  z-index: 100;
  padding: 8px;
  position: absolute;
  top: 60px;
  right: 0;
  width: 180px;
  background-color: ${Color.White};
  box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.15);
  border-radius: 4px;

  @media screen and (min-width: 768px) {
    top: 71px;
    width: 240px;
  }
`;

const DropdownList = styled.ul`
  padding: 8px 0;
  border-bottom: 1px solid ${Color.LightGrey2};
`;

const MenuItem = styled.li`
  ${Body1};
  color: ${Color.DarkGrey};
  width: 100%;
  padding: 9px 8px;
  box-sizing: border-box;

  &:hover {
    font-weight: bold;
  }
`;

const ProfileAfterLogin = ({ username, profileImg }) => {
  const [userCookies, setUserCookie, removeUserCookie] = useCookies([
    "userToken",
  ]);
  const handleLogout = () => {
    removeUserCookie(`userToken`, { path: "/" });
    window.location.reload();
  };
  const dropdownRef = useRef();
  const [isActive, setIsActive] = useDetectOutsideClick(dropdownRef, false);

  const showMenu = () => {
    setIsActive(!isActive);
  };

  return (
    <Container>
      <ProfileButton onClick={showMenu}>
        <ImgWrap>
          <Img src={profileImg === "" ? basicProfile : profileImg} />
        </ImgWrap>
      </ProfileButton>
      {isActive && (
        <Dropdown ref={dropdownRef} onClick={() => setIsActive(false)}>
          <DropdownList>
            <MenuItem>
              <SLink to={`/users/${username}`}>마이페이지</SLink>
            </MenuItem>
            <MenuItem>
              <SLink to="/upload">업로드</SLink>
            </MenuItem>
          </DropdownList>
          <LogoutButton onClick={handleLogout}>로그아웃</LogoutButton>
        </Dropdown>
      )}
    </Container>
  );
};

ProfileAfterLogin.propTypes = {
  username: PropTypes.string,
  profileImg: PropTypes.string,
};

export default ProfileAfterLogin;
