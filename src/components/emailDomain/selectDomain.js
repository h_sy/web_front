import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import arrowDown from "../../img/Icons/arrow_down.png";
import { propTypes } from "react-bootstrap/esm/Image";
import { Body1, Color } from "../mixIn";

const Select = styled.select`
  ${Body1};
  color: ${Color.DarkGrey};
  flex: 1;
  padding: 7px 6px 7px 11px;
  border: 1px solid ${Color.Grey1};
  box-sizing: border-box;
  border-radius: 4px;
  background: url(${arrowDown}) no-repeat 95% 50%/10px 10px;
  background-color: #ffffff;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
`;

const Option = styled.option``;

const SelectDomain = ({
  isDirect = false,
  setIsDirect,
  emailDomain = "default",
  setEmailDomain,
}) => {
  const handleChange = (e) => {
    if (e.target.value === "directInput") {
      setEmailDomain("");
      setIsDirect(true);
    } else {
      setEmailDomain(e.target.value);
    }
  };

  return (
    <Select
      defaultValue={isDirect ? "default" : emailDomain}
      onChange={handleChange}
    >
      <Option value="default" disabled>
        선택하세요
      </Option>
      <Option value="naver.com">naver.com</Option>
      <Option value="gmail.com">gmail.com</Option>
      <Option value="hanmail.net">hanmail.net</Option>
      <Option value="daum.net">daum.net</Option>
      <Option value="nate.com">nate.com</Option>
      <Option value="directInput">직접입력</Option>
    </Select>
  );
};

SelectDomain.propTypes = {
  isDirect: PropTypes.bool,
  setIsDirect: PropTypes.func.isRequired,
  emailDomain: PropTypes.string,
  setEmailDomain: PropTypes.func.isRequired,
};

export default SelectDomain;
