import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import cancelIcon from "../../img/cancel.png";
import { Body1, Color } from "../mixIn";

const Container = styled.div`
  flex: 1;
  position: relative;
`;

const Input = styled.input`
  ${Body1};
  color: ${Color.DarkGrey};
  width: 100%;
  padding: 7px 30px 7px 11px;
  border: 1px solid ${Color.Grey1};
  box-sizing: border-box;
  border-radius: 4px;
  margin-left: -1px;
  outline: none;

  &::placeholder {
    color: ${Color.Grey1};
  }
`;

const Button = styled.button`
  position: absolute;
  right: 1px;
  top: 0;
  padding: 18px 15px;
  background: url(${cancelIcon}) no-repeat 10px 13px/10px 10px;
`;

const DirectDomain = ({
  isDirect = false,
  setIsDirect,
  emailDomain = "",
  setEmailDomain,
}) => {
  return (
    <Container>
      <Input
        type="text"
        value={isDirect ? emailDomain : ""}
        onChange={(e) => setEmailDomain(e.currentTarget.value)}
        placeholder="입력하세요"
      />
      <Button onClick={() => setIsDirect(false)} />
    </Container>
  );
};

DirectDomain.propTypes = {
  isDirect: PropTypes.bool,
  setIsDirect: PropTypes.func.isRequired,
  emailDomain: PropTypes.string,
  setEmailDomain: PropTypes.func.isRequired,
};

export default DirectDomain;
