import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import PropTypes from "prop-types";

const SLink = styled(Link)`
  flex: 0 0 33.333%;
  box-sizing: border-box;
  padding: 2px;
  cursor: pointer;

  @media screen and (min-width: 768px) {
    padding: 8px;
    flex: 0 0 33.333%;
  }

  @media screen and (min-width: 1024px) {
    padding: 8px;
    flex: 0 0 25%;
  }
`;

const ImgWrap = styled.div`
  width: 100%;
  position: relative;

  &:after {
    content: "";
    display: block;
    padding-bottom: 100%;
  }
`;

const Img = styled.img`
  position: absolute;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const SquareImg = ({ itemRef, item }) => {
  return item === undefined ? null : (
    <SLink
      to={{
        pathname: `/contents/${item.id}`,
        state: { itemId: item.id },
      }}
    >
      <ImgWrap ref={itemRef}>
        <Img loading="lazy" src={item.images[0]} />
      </ImgWrap>
    </SLink>
  );
};

SquareImg.propTypes = {
  item: PropTypes.object.isRequired,
};

export default SquareImg;
