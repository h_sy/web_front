import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const List = styled.ul`
  @media screen and (min-width: 768px) {
    max-width: 800px;
    margin: 0 auto;
  }
`;

function FollowList({ children }) {
  return <List>{children}</List>;
}

FollowList.propTypes = {};

export default FollowList;
