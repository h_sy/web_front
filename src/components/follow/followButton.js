import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { LoginContext } from "../main";
import { userApi } from "../../api";
import { Color, Button1, Button2 } from "../mixIn";
import checkIcon from "../../img/Icons/check.svg";

const ButtonFollow = styled.button`
  ${Button2};
  background-color: ${Color.DeskGreen};
  color: ${Color.White};
  width: 90px;
  height: 40px;
  border-radius: 4px;
  z-index: 10;
  box-sizing: border-box;

  @media screen and (min-width: 768px) {
    ${Button1};
    width: 112px;
  }

  @media screen and (min-width: 1024px) {
    width: 100%;
  }
`;
const ButtonUnfollow = styled.button`
  ${Button2};
  background-color: ${Color.White};
  color: ${Color.Grey3};
  width: 90px;
  height: 40px;
  border: 1px solid ${Color.LightGrey2};
  border-radius: 4px;
  z-index: 10;
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  div {
    width: 18px;
    height: 18px;
    margin-right: 4px;

    img {
      width: 100%;
    }
  }

  @media screen and (min-width: 768px) {
    ${Button1};
    width: 112px;

    div {
      width: 20px;
      height: 20px;
    }
  }

  @media screen and (min-width: 1024px) {
    width: 100%;
  }
`;

function FollowButton({ userId, history }) {
  const { accessToken } = useContext(LoginContext);
  const [isFollowed, setIsFollowed] = useState(false);

  async function handleFollow(e, userId, accessToken) {
    e.preventDefault();
    if (userId === undefined || userId <= 0) return;
    else if (accessToken === undefined || accessToken === "") {
      alert("로그인이 필요합니다.");
      history.push("/login");
    } else {
      try {
        const response = await userApi.follow(userId, accessToken);
        if (response.status === 200) {
          setIsFollowed(true);
        }
      } catch (error) {
        console.log(error.response);
      }
    }
  }

  async function handleUnfollow(e, userId, accessToken) {
    e.preventDefault();
    if (userId === undefined || userId <= 0) return;
    else if (accessToken === undefined || accessToken === "") {
      alert("로그인이 필요합니다.");
      history.push("/login");
    } else {
      try {
        const response = await userApi.unfollow(userId, accessToken);
        if (response.status === 200) {
          setIsFollowed(false);
        }
      } catch (error) {
        console.log(error.response);
      }
    }
  }

  useEffect(() => {
    const checkIsFollowed = async (userId, accessToken) => {
      try {
        const { status, data } = await userApi.checkIsFollowed(
          userId,
          accessToken
        );
        if (status === 200) {
          setIsFollowed(data.followed);
        }
      } catch (error) {
        console.log(error.response);
      }
    };

    if (accessToken !== undefined && accessToken !== "") {
      checkIsFollowed(userId, accessToken);
    }
  }, [accessToken]);

  return isFollowed ? (
    <ButtonUnfollow onClick={(e) => handleUnfollow(e, userId, accessToken)}>
      <div>
        <img src={checkIcon} />
      </div>
      팔로잉
    </ButtonUnfollow>
  ) : (
    <ButtonFollow onClick={(e) => handleFollow(e, userId, accessToken)}>
      팔로우
    </ButtonFollow>
  );
}

FollowButton.propTypes = {
  userId: PropTypes.number.isRequired,
  history: PropTypes.object.isRequired,
};

export default FollowButton;
