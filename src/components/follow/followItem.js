import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import basicProfile from "../../img/Icons/btn_profile.svg";

const ListItem = styled.li`
  padding: 20px 0;
  border-bottom: 1px solid #e1e1e1;
`;

const SLink = styled(Link)``;

const Container = styled.div`
  display: flex;
  align-items: center;

  @media screen and (min-width: 768px) {
    width: 50%;
  }
`;

const ProfileImgWrap = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 100px;
  overflow: hidden;
  margin-right: 10px;

  @media screen and (min-width: 768px) {
    width: 58px;
    height: 58px;
  }
`;

const ProfileImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const UserInfoWrap = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: calc(100% - 120px);
  height: 40px;
  @media screen and (min-width: 768px) {
    width: calc(100% - 158px);
  }
`;

const Nickname = styled.h3`
  margin: 0;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  color: #333;
  font-size: 14px;
  font-weight: 500;
  line-height: 20px;

  @media screen and (min-width: 768px) {
    font-size: 18px;
    line-height: 27px;
  }
`;

const Job = styled.h4`
  margin: 0;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  color: #999;
  font-size: 11px;
  font-weight: 400;
  line-height: 17px;

  @media screen and (min-width: 768px) {
    font-size: 14px;
    line-height: 20px;
  }
`;

function FollowItem({ item }) {
  return (
    <ListItem>
      <SLink to={`/users/${item.username}`}>
        <Container>
          <ProfileImgWrap>
            <ProfileImg
              src={item.profileImg === "" ? basicProfile : item.profileImg}
            />
          </ProfileImgWrap>
          <UserInfoWrap>
            <Nickname>{item.nickname}</Nickname>
            <Job>{item.job}</Job>
          </UserInfoWrap>
        </Container>
      </SLink>
    </ListItem>
  );
}

FollowItem.propTypes = {
  item: PropTypes.object.isRequired,
};

export default FollowItem;
