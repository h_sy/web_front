import React, { createContext, useEffect, useState } from "react";
import styled from "styled-components";
import Routes from "./routes";
import Header from "./header/header";
import { useGetToken } from "../hooks/useGetToken";
import { userApi } from "../api";
import { Cookies, useCookies } from "react-cookie";

export const LoginContext = createContext({});
export const MyprofileContext = createContext({});

function Main() {
  const [userCookies, setUserCookie, removeUserCookie] = useCookies([
    "userToken",
  ]);
  const [accessToken, setAccessToken] = useState("");
  const [isLogin, setIsLogin] = useState(false);
  const [myProfile, setMyProfile] = useState({});

  useEffect(() => {
    const cookie = new Cookies();
    const access_token = cookie.get("userToken");
    if (access_token !== undefined) {
      setAccessToken(access_token);
    }
  }, []);

  useEffect(() => {
    const loginValidation = async (accessToken) => {
      try {
        const { status } = await userApi.tokenValidation(accessToken);
        if (status === 200) {
          setIsLogin(true);
        } else {
          setIsLogin(false);
        }
      } catch (error) {
        if (error.response.status === 401) {
          removeUserCookie(`userToken`, { path: "/" });
        } else {
          console.log(error.response);
        }
        setIsLogin(false);
      }
    };

    if (accessToken !== undefined && accessToken !== "") {
      loginValidation(accessToken);
    }
  }, [accessToken]);

  useEffect(() => {
    const getProfile = async (accessToken) => {
      try {
        const { data, status } = await userApi.getMyProfile(accessToken);

        if (status === 200) {
          setMyProfile(data);
        }
      } catch (error) {
        console.log(error);
      }
    };

    if (isLogin) {
      getProfile(accessToken);
    }
  }, [isLogin]);

  return (
    <LoginContext.Provider value={{ accessToken, setAccessToken, isLogin }}>
      <MyprofileContext.Provider value={{ myProfile }}>
        <Header />
        <Routes />
      </MyprofileContext.Provider>
    </LoginContext.Provider>
  );
}

export default Main;
