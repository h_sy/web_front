import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import NotFound from "../pages/404";
import ContentDetail from "../pages/contentDetail";
import ContentUpdate from "../pages/contentUpdate";
import Followee from "../pages/followPage/followee";
import Follower from "../pages/followPage/follower";
import Home from "../pages/home";
import Login from "../pages/login";
import MyPage from "../pages/myPage/myPage";
import MySetting from "../pages/myPage/mySetting/mySetting";
import Search from "../pages/search/search";
import SearchByContents from "../pages/search/searchByContents";
import SearchByJobs from "../pages/search/searchByJobs";
import SignUp from "../pages/signUp";
import Upload from "../pages/upload";
import PrivateRoute from "./privateRoute";

function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/login" exact component={Login} />
      <Route path="/signup" exact component={SignUp} />
      <Route path="/contents/:id" exact component={ContentDetail} />
      <Route path="/search" exact component={Search} />
      <Route path="/search/job" exact component={SearchByJobs} />
      <Route path="/search/content" exact component={SearchByContents} />
      <Route path="/users/:username" exact component={MyPage} />
      <Route path="/users/:userId/follower" exact component={Follower} />
      <Route path="/users/:userId/followee" exact component={Followee} />
      <PrivateRoute
        path="/users/:username/setting"
        exact
        component={MySetting}
      />
      <PrivateRoute path="/upload" exact component={Upload} />
      <PrivateRoute
        path="/contents/update/:id"
        exact
        component={ContentUpdate}
      />
      <Route component={NotFound} />
      <Redirect from="*" to="/" />
    </Switch>
  );
}

export default Routes;
