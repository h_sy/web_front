import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import PropTypes from "prop-types";
import basicProfile from "../../img/Icons/btn_profile.svg";
import { Color, elip2, elip1, Headline1, Headline2, Subtitle2 } from "../mixIn";

const SLink = styled(Link)`
  position: absolute;
  right: 20px;
  bottom: 20px;
  padding: 10px 20px;
  font-size: 14px;
  line-height: 22px;
  font-weight: bold;
  color: ${Color.White};
  border: 1px solid #ffffff;
  border-radius: 4px;
  text-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);

  @media screen and (min-width: 768px) {
    right: 40px;
    bottom: 40px;
    padding: 10px 30px;
    font-size: 16px;
    line-height: 24px;
  }

  @media screen and (min-width: 1024px) {
    padding: 14px 42px;
  }
`;

const Container = styled.div`
  position: relative;
`;

const ImgWrap = styled.div`
  overflow: hidden;
  height: 430px;
  @media screen and (min-width: 1440px) {
    height: 490px;
  }
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const CardInfo = styled.div`
  position: absolute;
  left: 20px;
  right: 20px;
  bottom: 20px;

  @media screen and (min-width: 768px) {
    left: 40px;
    right: 40px;
    bottom: 40px;
  }
`;

const Title = styled.h3`
  ${Headline2};
  color: ${Color.White};
  ${elip2};
  text-transform: capitalize;
  text-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
  margin-bottom: 16px;

  @media screen and (min-width: 768px) {
    font-size: 28px;
    line-height: 32px;
  }

  @media screen and (min-width: 1024px) {
    ${Headline1}
  }
`;

const Writer = styled.div`
  display: flex;
  align-items: center;
`;

const ProfileImgWrap = styled.div`
  width: 36px;
  height: 36px;
  border-radius: 100px;
  overflow: hidden;
  margin-right: 16px;
`;

const ProfileImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  vertical-align: top;
`;

const Nickname = styled.div`
  ${Subtitle2};
  color: ${Color.White};
  font-weight: 400;
  text-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
  ${elip1};
  width: 50%;
`;

const HashtagJob = styled.div`
  font-size: 16px;
  line-height: 24px;
  font-weight: bold;
  color: ${Color.White};
  border: 1px solid #ffffff;
  border-radius: 100px;
  display: inline-block;
  padding: 8px 16px;
  margin-bottom: 16px;
  text-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
`;

function BannerItem({ item }) {
  return (
    <Container>
      <ImgWrap>
        <Img loading="lazy" src={item.images[0]} />
      </ImgWrap>
      <CardInfo>
        <HashtagJob>#{item.job}</HashtagJob>
        <Title>{item.title}</Title>
        <Writer>
          <ProfileImgWrap>
            <ProfileImg
              src={
                item["profile_img"] === "" ? basicProfile : item["profile_img"]
              }
            />
          </ProfileImgWrap>
          <Nickname>{item.nickname}</Nickname>
        </Writer>
      </CardInfo>
      <SLink
        to={{
          pathname: `contents/${item.id}`,
          state: { itemId: item.id },
        }}
      >
        보러가기
      </SLink>
    </Container>
  );
}

BannerItem.propTypes = {
  item: PropTypes.object.isRequired,
};

export default BannerItem;
