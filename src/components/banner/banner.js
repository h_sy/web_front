import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { contentApi } from "../../api";
import BannerItem from "./bannerItem";
import { LoginContext } from "../main";
import SliderForBanner from "../slider/sliderForBanner";

const SliderWrap = styled.div`
  height: 430px;
  border-radius: 16px;
  overflow: hidden;

  @media screen and (min-width: 1440px) {
    height: 490px;
  }
`;

function Banner() {
  const { accessToken, isLogin } = useContext(LoginContext);
  const [data, setData] = useState([]);

  useEffect(() => {
    const getBestData = async () => {
      try {
        const response = await contentApi.getBestContents({
          accessToken: accessToken,
        });
        if (response.status === 200) {
          setData(response.data);
        }
      } catch (error) {
        console.log(error.response);
      }
    };

    getBestData();

    return () => setData(data);
  }, [isLogin]);

  return (
    <SliderWrap>
      <SliderForBanner>
        {data && data.map((item) => <BannerItem key={item.id} item={item} />)}
      </SliderForBanner>
    </SliderWrap>
  );
}

Banner.propTypes = {};

export default Banner;
