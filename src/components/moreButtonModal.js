import React, { useRef } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const Container = styled.div`
  padding: 8px;
`;

const ModalOverlay = styled.div`
  box-sizing: border-box;
  display: ${(props) => (props.visible ? "block" : "none")};
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 999;
  background-color: rgba(0, 0, 0, 0.6);
`;

const ModalContainer = styled.div`
  box-sizing: border-box;
  display: ${(props) => (props.visible ? "block" : "none")};
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1000;
  overflow: auto;
  outline: 0;
`;

const ModalInner = styled.div`
  box-sizing: border-box;
  position: relative;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.5);
  background-color: #ffffff;
  top: 50%;
  transform: translateY(-50%);
  margin: 0 auto;
  border-radius: 10px;
  width: 50%;
  height: 175px;
  max-width: 480px;
`;

function MoreButtonModal({ isActive, modalRef, children }) {
  return (
    <ModalOverlay visible={isActive}>
      <ModalContainer tabIndex="-1" visible={isActive}>
        <ModalInner ref={modalRef} tabIndex="0">
          <Container>{children}</Container>
        </ModalInner>
      </ModalContainer>
    </ModalOverlay>
  );
}

MoreButtonModal.propTypes = {};

export default MoreButtonModal;
