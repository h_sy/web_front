import { createGlobalStyle } from "styled-components";
import reset from "styled-reset";

const GlobalStyles = createGlobalStyle`
  ${reset};

  a{
      text-decoration:none;
      color:inherit;
  }

  button {
    cursor: pointer;
    background-color: transparent;
    outline: 0;
    border: 0;
  }

  *{
      box-sizing:border-box;
      font-family: 'Spoqa Han Sans Neo', 'sans-serif';
  }

  body {
    background-color: #ffffff;
    font-family: 'Spoqa Han Sans Neo', 'sans-serif';
    min-width: 320px;
    height:100%;
  }

  html, #root, .App {
    height:100%;
  }

  .App {
    display:flex;
    flex-direction:column;
  }
`;

export default GlobalStyles;
