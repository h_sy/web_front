import { useEffect, useState } from "react";
import { contentApi } from "../api";

export default function useGetRandom(page, accessToken = "") {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [data, setData] = useState([]);
  const [hasMore, setHasMore] = useState(false);

  async function getFirstRandomData(accessToken) {
    setLoading(true);
    setError(false);
    try {
      const response = await contentApi.getRandom({ page: 0, accessToken });
      if (response.status === 200) {
        setData(response.data);
        setHasMore(response.data.length > 0);
      }
    } catch (error) {
      console.log(error.response);
      setError(true);
    } finally {
      setLoading(false);
    }
  }

  async function getRandomData(page, accessToken) {
    setLoading(true);
    setError(false);
    try {
      const response = await contentApi.getRandom({ page, accessToken });
      if (response.status === 200) {
        setData((prevData) => [...prevData, ...response.data]);
        setHasMore(response.data.length > 0);
      }
    } catch (error) {
      console.log(error.response);
      setError(true);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    if (page > 0) {
      getRandomData(page, accessToken);
    }
  }, [page]);

  useEffect(() => {
    getFirstRandomData(accessToken);
  }, [accessToken]);

  return { loading, error, data, hasMore };
}
