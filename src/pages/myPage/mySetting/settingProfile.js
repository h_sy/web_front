import React, { useContext, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import plusIcon from "../../../img/Icons/plus.svg";
import basicProfile from "../../../img/Icons/btn_profile.svg";
import SelectDomain from "../../../components/emailDomain/selectDomain";
import DirectDomain from "../../../components/emailDomain/directDomain";
import InputSns from "../../../components/inputSns";
import { userApi } from "../../../api";
import { LoginContext } from "../../../components/main";
import {
  Body1,
  Body3,
  Button1,
  Color,
  Headline2,
} from "../../../components/mixIn";

const Container = styled.div`
  padding: 40px 30px;
  max-width: 700px;
  margin: 0 auto;
`;

const Title = styled.h1`
  ${Headline2};
  color: ${Color.DarkGrey};
  text-align: center;
  margin-bottom: 40px;
`;

const Form = styled.form``;

const SetImgWrap = styled.div`
  margin-top: 40px;
`;

const ImgOuterWrap = styled.div`
  position: relative;
  width: 100px;
  margin: 0 auto;
`;

const ImgWrap = styled.div`
  width: 100px;
  height: 100px;
  border-radius: 100px;
  margin: 0 auto;
  overflow: hidden;
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const ImgLabel = styled.label`
  cursor: pointer;
  margin: 15px auto 0;
  display: block;
  width: 150px;
  text-align: center;
  ${Body3};
  color: ${Color.DeskGreen};
`;

const RemoveImgButton = styled.button`
  font-size: 15px;
  position: absolute;
  top: -4px;
  right: -10px;
`;

const InputFile = styled.input`
  overflow: hidden;
  position: absolute;
  clip: rect(0 0 0 0);
  width: 1px;
  height: 1px;
  margin: -1px;
`;

const InputWrap = styled.div`
  margin-top: 40px;
  display: flex;
  position: relative;
  flex-direction: row;
  align-items: flex-start;
`;

const Input = styled.input`
  ${Body1};
  color: ${Color.DarkGrey};
  width: 100%;
  padding: 7px 11px;
  border: 1px solid ${Color.Grey1};
  box-sizing: border-box;
  border-radius: 4px;
  margin-left: -1px;
  ${(props) => (props.flex ? "flex:1" : null)};
  outline: none;

  &::placeholder {
    color: ${Color.Grey1};
  }
`;

const Label = styled.label`
  ${Body1};
  color: ${Color.DarkGrey};
  width: 100px;
  display: inline-block;
`;

const EmailWrap = styled.div`
  display: flex;
  width: 100%;
`;

const AtSign = styled.span`
  color: #8e8e8e;
  display: inline-block;
  width: 26px;
  padding: 10px 6px;
`;

const AddSnsButton = styled.button`
  position: absolute;
  top: -4px;
  left: 35px;
  background: url(${plusIcon}) no-repeat 9px 9px/ 12px 12px;
  padding: 15px;
`;

const Textarea = styled.textarea`
  ${Body1};
  resize: none;
  min-height: 100px;
  width: 100%;
  padding: 11px;
  border: 1px solid ${Color.Grey1};
  box-sizing: border-box;
  border-radius: 4px;
  margin-left: -1px;
  outline: none;

  &::placeholder {
    color: ${Color.Grey1};
  }
`;

const SubmitButton = styled.button`
  ${Button1};
  color: ${Color.White};
  width: 100%;
  background-color: ${Color.DeskGreen};
  text-align: center;
  padding: 15px 0;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 40px;
`;

const SnsWrap = styled.div`
  width: 100%;
`;

const REGISTERED_DOMAINS = [
  "naver.com",
  "gmail.com",
  "hanmail.net",
  "daum.net",
  "nate.com",
];

const SettingProfile = ({ userProfile, history }) => {
  const { accessToken } = useContext(LoginContext);
  const {
    username,
    profileImg,
    nickname,
    email,
    job,
    location,
    introduction,
    snsBundle,
  } = userProfile;

  const userEmail = email.split("@");
  const [userImg, setUserImg] = useState(profileImg);
  const [userNickname, setUserNickname] = useState(nickname);
  const [emailId, setEmailId] = useState(userEmail[0]);
  const [emailDomain, setEmailDomain] = useState(userEmail[1]);
  const [userJob, setUserJob] = useState(job);
  const [userLocation, setUserLocation] = useState(location);
  const [userIntroduction, setUserIntroduction] = useState(introduction);
  const [userSns, setUserSns] = useState(snsBundle);

  const [isDirect, setIsDirect] = useState(() => {
    if (!REGISTERED_DOMAINS.includes(emailDomain)) {
      return true;
    } else {
      return false;
    }
  });

  const deleteImgClick = (e) => {
    e.preventDefault();
    setUserImg("");
  };

  const addSnsClick = (e) => {
    e.preventDefault();
    if (userSns.length >= 3) return;
    else {
      setUserSns([...userSns, ""]);
    }
  };

  const removeSnsClick = (e, index) => {
    e.preventDefault();
    if (userSns.length <= 0) return;
    else {
      setUserSns(userSns.filter((item, idx) => idx !== index));
    }
  };

  const updateSns = (e, index) => {
    setUserSns(
      userSns.map((item, idx) => (idx === index ? e.target.value : item))
    );
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    if (typeof userImg !== "string") {
      formData.append("profileImg", userImg);
    }
    formData.append("nickname", userNickname);
    formData.append("email", `${emailId}@${emailDomain}`);
    formData.append("job", userJob);
    formData.append("location", userLocation);
    formData.append("introduction", userIntroduction);
    userSns.forEach((item) => {
      if (item !== "") {
        formData.append("snsBundle", item);
      }
    });

    try {
      const response = await userApi.updateProfile(formData, accessToken);
      if (response.status === 200) {
        alert("성공적으로 수정되었습니다😊");
        history.push(`/users/${username}`);
      }
    } catch (error) {
      console.log(error.response);
    }
  };

  return (
    <Container>
      <Title>회원정보 수정</Title>
      <Form onSubmit={handleSubmit}>
        <SetImgWrap>
          <ImgOuterWrap>
            <ImgWrap>
              <Img
                src={
                  userImg === ""
                    ? basicProfile
                    : typeof userImg === "file" || typeof userImg === "object"
                    ? URL.createObjectURL(userImg)
                    : userImg
                }
              />
            </ImgWrap>
            <RemoveImgButton onClick={(e) => deleteImgClick(e)}>
              ❌
            </RemoveImgButton>
          </ImgOuterWrap>
          <ImgLabel htmlFor="user_profileImg">프로필 이미지 변경</ImgLabel>
          <InputFile
            type="file"
            id="user_profileImg"
            accept=".jpg, .png"
            onChange={(e) => setUserImg(e.target.files[0])}
          />
        </SetImgWrap>
        <InputWrap>
          <Label htmlFor="user_nickname">닉네임*</Label>
          <Input
            type="text"
            id="user_nickname"
            placeholder="닉네임"
            value={userNickname}
            onChange={(e) => setUserNickname(e.currentTarget.value)}
            required
          />
        </InputWrap>
        <InputWrap>
          <Label htmlFor="user_email">이메일</Label>
          <EmailWrap>
            <Input
              type="text"
              id="user_email"
              placeholder="이메일"
              flex={true}
              value={emailId}
              onChange={(e) => setEmailId(e.currentTarget.value)}
            />
            <AtSign>@</AtSign>
            {isDirect ? (
              <DirectDomain
                isDirect={isDirect}
                setIsDirect={setIsDirect}
                emailDomain={emailDomain}
                setEmailDomain={setEmailDomain}
              />
            ) : (
              <SelectDomain
                isDirect={isDirect}
                setIsDirect={setIsDirect}
                emailDomain={emailDomain}
                setEmailDomain={setEmailDomain}
              />
            )}
          </EmailWrap>
        </InputWrap>
        <InputWrap>
          <Label htmlFor="user_job">직업</Label>
          <Input
            type="text"
            id="user_job"
            value={userJob}
            placeholder="직업"
            onChange={(e) => setUserJob(e.currentTarget.value)}
          />
        </InputWrap>
        <InputWrap>
          <Label htmlFor="user_location">지역</Label>
          <Input
            type="text"
            id="user_location"
            value={userLocation}
            placeholder="지역"
            onChange={(e) => setUserLocation(e.currentTarget.value)}
          />
        </InputWrap>
        <InputWrap>
          <Label htmlFor="user_introduction">소개</Label>
          <Textarea
            placeholder="소개글을 입력하세요 :)"
            value={userIntroduction}
            onChange={(e) => setUserIntroduction(e.currentTarget.value)}
          />
        </InputWrap>
        <InputWrap>
          <Label htmlFor="user_sns">SNS</Label>
          <AddSnsButton onClick={addSnsClick} />
          <SnsWrap>
            {userSns.map((item, idx) => (
              <InputSns
                key={idx}
                sns={item}
                updateSns={(e) => updateSns(e, idx)}
                removeSnsClick={(e) => removeSnsClick(e, idx)}
              />
            ))}
          </SnsWrap>
        </InputWrap>
        <SubmitButton onSubmit={handleSubmit}>회원정보수정</SubmitButton>
      </Form>
    </Container>
  );
};

SettingProfile.propTypes = {
  userProfile: PropTypes.object.isRequired,
};

export default SettingProfile;
