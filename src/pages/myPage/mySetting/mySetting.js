import React, { useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import ChangePassword from "./changePassword";
import SettingProfile from "./settingProfile";
import Withdrawal from "./withdrawal";
import { Color, Button1, Button2 } from "../../../components/mixIn";

const Container = styled.div`
  width: 100%;
  max-width: 700px;
  margin: 0 auto;
`;

const List = styled.ul`
  border-bottom: 1px solid #e8e8e8;
  display: flex;
  justify-content: space-evenly;
`;

const Item = styled.li`
  cursor: pointer;
  text-align: center;

  span {
    ${Button2};
    color: ${(props) =>
      props.active ? `${Color.DeskGreen}` : `${Color.Grey2}`};
    display: inline-block;
    padding: 10px 18px;
    border-bottom: ${(props) =>
      props.active ? `4px solid ${Color.DeskGreen};` : 0};
  }

  @media screen and (min-width: 768px) {
    span {
      ${Button1};
      padding: 14px 32px;
    }
  }
`;

const MySetting = ({ location, history }) => {
  const { userProfile } = location.state;
  const [activeTab, setActiveTab] = useState("settingProfile");
  const obj = {
    settingProfile: (
      <SettingProfile userProfile={userProfile} history={history} />
    ),
    changePassword: <ChangePassword />,
    withdrawal: <Withdrawal />,
  };

  const handleClick = (tabId) => {
    setActiveTab(tabId);
  };

  return (
    <Container>
      <List>
        <Item
          active={activeTab === "settingProfile"}
          onClick={() => handleClick("settingProfile")}
        >
          <span>회원정보수정</span>
        </Item>
        <Item
          active={activeTab === "changePassword"}
          onClick={() => handleClick("changePassword")}
        >
          <span>비밀번호 변경</span>
        </Item>
        <Item
          active={activeTab === "withdrawal"}
          onClick={() => handleClick("withdrawal")}
        >
          <span>회원탈퇴</span>
        </Item>
      </List>
      {obj[activeTab]}
    </Container>
  );
};

MySetting.propTypes = {};

export default MySetting;
