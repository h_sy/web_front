import React, { useContext, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { userApi } from "../../../api";
import { LoginContext } from "../../../components/main";
import {
  Body1,
  Button1,
  Caption,
  Color,
  Headline2,
} from "../../../components/mixIn";

const Container = styled.div`
  padding: 40px 30px;
  max-width: 500px;
  margin: 0 auto;
`;

const Title = styled.h1`
  ${Headline2};
  color: ${Color.DarkGrey};
  text-align: center;
  margin-bottom: 40px;
`;

const Form = styled.form``;

const InputWrap = styled.div`
  margin-top: 30px;
`;

const Input = styled.input`
  ${Body1};
  color: ${Color.DarkGrey};
  width: 100%;
  padding: 7px 11px;
  border: 1px solid ${Color.Grey1};
  box-sizing: border-box;
  border-radius: 4px;
  margin-left: -1px;
  outline: none;

  &::placeholder {
    color: ${Color.Grey1};
  }
`;

const Label = styled.label`
  ${Body1};
  color: ${Color.DarkGrey};
  display: block;
`;

const InputInfo = styled.div`
  ${Caption};
  color: ${Color.Grey2};
  margin-bottom: 6px;
`;

const SubmitButton = styled.button`
  ${Button1};
  color: ${Color.White};
  width: 100%;
  background-color: ${Color.DeskGreen};
  text-align: center;
  padding: 15px 0;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 40px;
`;

const CheckMsg = styled.div`
  ${Caption};
  color: #e00000;
  margin-top: 5px;
`;

const ChangePassword = () => {
  const { accessToken } = useContext(LoginContext);
  const [curPassword, setCurPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [checkPassword, setCheckPassword] = useState("");

  const PasswordCheckMsg = () => {
    if (checkPassword) {
      if (newPassword !== checkPassword) {
        return <CheckMsg>패스워드가 일치하지 않습니다</CheckMsg>;
      }
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (curPassword === "" || newPassword === "" || checkPassword === "")
      return;
    if (newPassword !== checkPassword) {
      alert("새 비밀번호가 일치하지 않습니다.");
      return;
    }
    try {
      const response = await userApi.changePassword(
        accessToken,
        curPassword,
        newPassword
      );
      if (response.status === 200) {
        alert("비밀번호가 변경되었습니다.🥳");
        window.location.reload();
      }
    } catch (error) {
      console.log(error.response);
      if (error.response.status === 401) {
        alert("기존 비밀번호가 일치하지 않습니다.");
      } else if (error.response.status === 400) {
        alert("새 비밀번호를 입력하세요.");
      }
    }
  };

  return (
    <Container>
      <Title>비밀번호 변경</Title>
      <Form onSubmit={handleSubmit}>
        <InputWrap>
          <Label htmlFor="user_currentPassword">기존 비밀번호 *</Label>
          <Input
            type="password"
            id="user_currentPassword"
            placeholder="기존 비밀번호"
            value={curPassword}
            minLength="8"
            required
            onChange={(e) => setCurPassword(e.currentTarget.value)}
          />
        </InputWrap>
        <InputWrap>
          <Label htmlFor="user_password">새 비밀번호 *</Label>
          <InputInfo>8자 이상 입력해주세요.</InputInfo>
          <Input
            type="password"
            id="user_password"
            placeholder="새 비밀번호"
            value={newPassword}
            minLength="8"
            required
            onChange={(e) => setNewPassword(e.currentTarget.value)}
          />
        </InputWrap>
        <InputWrap>
          <Label htmlFor="user_checkPassword">비밀번호 확인*</Label>
          <Input
            type="password"
            id="user_checkPassword"
            placeholder="비밀번호 확인"
            value={checkPassword}
            minLength="8"
            required
            onChange={(e) => setCheckPassword(e.currentTarget.value)}
          />
          {PasswordCheckMsg()}
        </InputWrap>
        <SubmitButton onClick={handleSubmit}>비밀번호 변경</SubmitButton>
      </Form>
    </Container>
  );
};

ChangePassword.propTypes = {};

export default ChangePassword;
