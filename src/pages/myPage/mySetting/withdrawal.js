import React, { useState } from "react";
import styled from "styled-components";
import {
  Body1,
  Body3,
  Color,
  Headline2,
  Subtitle2,
} from "../../../components/mixIn";
import checkIcon from "../../../img/Icon/checked.png";
import notCheckIcon from "../../../img/Icon/not checked.png";

const Container = styled.div`
  padding: 40px 30px;
  max-width: 700px;
  margin: 0 auto;
`;

const Title = styled.h1`
  ${Headline2};
  color: ${Color.DarkGrey};
  text-align: center;
  margin-bottom: 40px;
`;

const Wrap = styled.div``;

const Subtitle = styled.h2`
  ${Subtitle2};
  color: ${Color.Grey3};
  margin: 15px 0;
`;

const ContentWrap = styled.div`
  padding: 20px;
  border: 1px solid ${Color.LightGrey2};
  box-sizing: border-box;
  border-radius: 16px;
`;

const ContentTitle = styled.h3`
  ${Body1};
  color: ${Color.DarkGrey};
  margin-bottom: 10px;
`;

const ContentP = styled.p`
  ${Body3};
  color: ${Color.Grey2};
`;

const CheckboxWrap = styled.div`
  padding: 10px 0;
`;

const Input = styled.input``;

const Label = styled.label`
  ${Body3};
  color: ${Color.DarkGrey};
  vertical-align: middle;
`;

const Checkbox = styled.a`
  display: inline-block;
  width: 22px;
  height: 22px;
  background: url(${(props) => (props.checked ? checkIcon : notCheckIcon)})
    no-repeat 0 0/22px 22px;
  vertical-align: middle;
  margin-right: 5px;
`;

const AlertMessage = styled.span`
  display: inline-block;
  margin-left: 5px;
  color: ${(props) => (props.needed ? "#ff7777" : "#777777")};
`;

const Withdrawal = () => {
  const [confirmChecked, setConfirmChecked] = useState(false);
  const [reasonChecked, setReasonChecked] = useState([
    {
      id: "reason0",
      value: "이용빈도 낮음",
      checked: false,
    },
    {
      id: "reason1",
      value: "재가입",
      checked: false,
    },
    {
      id: "reason2",
      value: "콘텐츠 부족",
      checked: false,
    },
    {
      id: "reason3",
      value: "개인정보보호",
      checked: false,
    },
    {
      id: "reason4",
      value: "기타",
      checked: false,
    },
  ]);

  const handleReasonClick = (targetId) => {
    const newReasons = reasonChecked;
    newReasons.forEach((reason) => {
      if (reason.id === targetId) {
        reason.checked = !reason.checked;
      }
    });
    setReasonChecked(newReasons);
  };
  return (
    <Container>
      <Title>회원탈퇴</Title>
      <Wrap>
        <Subtitle>회원탈퇴에 앞서 아래 내용을 반드시 확인해 주세요.</Subtitle>
        <ContentWrap>
          <ContentTitle>회원탈퇴 시 게시물 관리</ContentTitle>
          <ContentP>
            회원탈퇴 후 데스크홀릭 서비스에 입력한 게시물 및 댓글은 삭제되지
            않으며, 회원정보 삭제로 인해 작성자 본인을 확인할 수 없으므로 게시물
            편집 및 삭제 처리가 원천적으로 불가능 합니다. 게시물 삭제를 원하시는
            경우에는 먼저 해당 게시물을 삭제 하신 후, 탈퇴를 신청하시기
            바랍니다.
          </ContentP>
        </ContentWrap>
        <CheckboxWrap>
          <Input
            id="confirmRead"
            type="checkbox"
            checked={confirmChecked}
            className="blind"
            value="위 내용을 모두 확인하였습니다."
            onChange={() => {}}
          />
          <Checkbox
            checked={confirmChecked}
            onClick={() => setConfirmChecked(!confirmChecked)}
          />
          <Label
            htmlFor="confirmRead"
            onClick={() => setConfirmChecked(!confirmChecked)}
          >
            위 내용을 모두 확인하였습니다.
            <AlertMessage needed={true}>필수</AlertMessage>
          </Label>
        </CheckboxWrap>
      </Wrap>
      <Wrap>
        <Subtitle>
          데스크홀릭 회원에서 탈퇴하려는 이유가 무엇인가요? (복수선택 가능)
        </Subtitle>
        <ContentWrap>
          {reasonChecked.map((reason, idx) => (
            <CheckboxWrap key={idx}>
              <Input
                id={reason.id}
                type="checkbox"
                value={reason.value}
                defaultChecked={reason.checked}
                onChange={() => {}}
              />
              <Label
                htmlFor={reason.id}
                onClick={() => handleReasonClick(reason.id)}
              >
                <Checkbox
                  checked={reason.checked}
                  onClick={() => handleReasonClick(reason.id)}
                />
                {reason.value}
              </Label>
            </CheckboxWrap>
          ))}
        </ContentWrap>
      </Wrap>
    </Container>
  );
};

export default Withdrawal;
