import React, { createContext, useContext, useEffect, useState } from "react";
import styled from "styled-components";
import jwt_decode from "jwt-decode";
import MyProfile from "./myProfile/myProfile";
import { LoginContext, MyprofileContext } from "../../components/main";
import { userApi } from "../../api";
import HelmetForSEO from "../../components/helmetForSeo";

const Container = styled.div`
  width: 100%;
  max-width: 960px;
  margin: 0 auto;
`;

export const MypageContext = createContext({});

const MyPage = ({ match, history }) => {
  const {
    params: { username },
  } = match;
  const { accessToken, isLogin } = useContext(LoginContext);
  const { myProfile } = useContext(MyprofileContext);

  const [userProfile, setUserProfile] = useState({});
  const [isMypage, setIsMypage] = useState(false);

  useEffect(() => {
    async function getProfile(username) {
      try {
        const { status, data } = await userApi.getProfile(username);
        if (status === 200) {
          setUserProfile(data);
        }
      } catch (error) {
        console.log(error.response);
      }
    }

    if (isLogin && accessToken !== undefined && accessToken !== "") {
      const { username: decodedUsername } = jwt_decode(accessToken);

      if (decodedUsername === username) {
        setIsMypage(true);
        setUserProfile(myProfile);
      } else {
        setIsMypage(false);
        getProfile(username);
      }
    } else {
      setIsMypage(false);
      getProfile(username);
    }
  }, [isLogin, myProfile, username]);

  return (
    <>
      <HelmetForSEO
        title={`${userProfile.nickname}님의 작업공간 ! | DESK Holic`}
        description={userProfile.introduction}
        image={userProfile.profileImg}
        url={`users/${username}`}
      />
      <Container>
        <MypageContext.Provider value={{ isMypage, username }}>
          <MyProfile userProfile={userProfile} history={history} />
        </MypageContext.Provider>
      </Container>
    </>
  );
};

export default MyPage;
