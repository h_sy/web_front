import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import styled from "styled-components";
import { contentApi } from "../../../api";
import LoaderBottom from "../../../components/loaderBottom";
import SquareImg from "../../../components/squareImg";
import { MypageContext } from "../myPage";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  padding: 2px;

  @media screen and (min-width: 768px) {
    padding: 8px;
  }

  @media screen and (min-width: 1024px) {
    padding: 52px 0;
    margin: 0 -8px;
  }
`;

const LoadingMsg = styled.div`
  font-size: 30px;
`;

const OPTIONS = {
  thredhold: 0.5,
  rootMargin: "100px",
};

const ViewMyLikes = () => {
  const { username } = useContext(MypageContext);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [page, setPage] = useState(0);
  const [hasMore, setHasMore] = useState(false);

  const observer = useRef();
  const lastElementRef = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPage((prevPage) => prevPage + 1);
        }
      }, OPTIONS);
      if (node) observer.current.observe(node);
    },
    [loading, hasMore]
  );

  useEffect(() => {
    setPage(0);
    setData([]);
  }, [username]);

  useEffect(() => {
    async function getUserLikes(username, page) {
      setLoading(true);
      try {
        const response = await contentApi.getAllLikes({ username, page });
        if (response.status === 200) {
          setData((prevData) => [...prevData, ...response.data]);
          setHasMore(response.data.length > 0);
        }
      } catch (error) {
        console.log(error.response);
      } finally {
        setLoading(false);
      }
    }

    getUserLikes(username, page);
  }, [username, page]);

  return (
    <>
      <Container>
        {data &&
          data.length > 0 &&
          data.map((item, idx) => {
            if (data.length === idx + 1) {
              return (
                <SquareImg itemRef={lastElementRef} key={item.id} item={item} />
              );
            } else {
              return <SquareImg key={item.id} item={item} />;
            }
          })}
      </Container>
      <LoadingMsg>{loading && <LoaderBottom />}</LoadingMsg>
    </>
  );
};

export default ViewMyLikes;
