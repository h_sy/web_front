import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { contentApi } from "../../../api";
import SquareImg from "../../../components/squareImg";
import { MypageContext } from "../myPage";
import { Button1, Button2, Color } from "../../../components/mixIn";
import plusIcon from "../../../img/Icons/plus.svg";
import { Link } from "react-router-dom";
import LoaderBottom from "../../../components/loaderBottom";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  padding: 2px;

  @media screen and (min-width: 768px) {
    padding: 8px;
  }

  @media screen and (min-width: 1024px) {
    padding: 52px 0;
    margin: 0 -8px;
  }
`;

const LoadingMsg = styled.div`
  font-size: 30px;
`;

const UploadButton = styled(Link)`
  border: 1px dashed ${Color.LightGrey2};
  box-sizing: border-box;
  border-radius: 16px;
  flex: 0 0 33.333%;
  position: relative;

  &::before {
    content: "";
    display: block;
    padding-top: 100%;
  }

  div {
    display: flex;
    flex-direction: column;
    align-items: center;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 100%;

    img {
      width: 18px;
      height: 18px;
    }

    span {
      ${Button2};
      color: ${Color.Grey2};
      margin-top: 10px;
    }
  }

  @media screen and (min-width: 768px) {
    padding: 8px;

    div {
      img {
        width: 24px;
        height: 24px;
      }

      span {
        ${Button1};
        color: ${Color.Grey2};
        margin-top: 16px;
      }
    }
  }

  @media screen and (min-width: 1024px) {
    flex: 0 0 25%;
  }
`;

const OPTIONS = {
  thredhold: 0.5,
  rootMargin: "100px",
};

const ViewMyUploads = () => {
  const { username, isMypage } = useContext(MypageContext);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [page, setPage] = useState(0);
  const [hasMore, setHasMore] = useState(false);

  const observer = useRef();
  const lastElementRef = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPage((prevPage) => prevPage + 1);
        }
      }, OPTIONS);
      if (node) observer.current.observe(node);
    },
    [loading, hasMore, username]
  );

  useEffect(() => {
    setPage(0);
    setData([]);
  }, [username]);

  useEffect(() => {
    async function getUserUploads(username, page) {
      setLoading(true);
      try {
        const response = await contentApi.getAll({ username, page });
        if (response.status === 200) {
          setData((prevData) => [...prevData, ...response.data]);
          setHasMore(response.data.length > 0);
        }
      } catch (error) {
        console.log(error.response);
      } finally {
        setLoading(false);
      }
    }

    getUserUploads(username, page);
  }, [username, page]);

  return (
    <>
      <Container>
        {data &&
          data.length > 0 &&
          data.map((item, idx) => {
            if (data.length === idx + 1) {
              return (
                <SquareImg itemRef={lastElementRef} key={item.id} item={item} />
              );
            } else {
              return <SquareImg key={item.id} item={item} />;
            }
          })}
        {isMypage && data && data.length <= 0 && (
          <UploadButton to={"/upload"}>
            <div>
              <img src={plusIcon} />
              <span>사진 올리기</span>
            </div>
          </UploadButton>
        )}
      </Container>
      <LoadingMsg>{loading && <LoaderBottom />}</LoadingMsg>
    </>
  );
};

ViewMyUploads.propTypes = {};

export default ViewMyUploads;
