import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import ProfileCard from "../../../components/profileCard";
import MyProfileTab from "../../../components/myProfileTab";

const Container = styled.div``;

const MyProfile = ({ userProfile, history }) => {
  return (
    <Container>
      <ProfileCard userProfile={userProfile} history={history} />
      <MyProfileTab />
    </Container>
  );
};

MyProfile.propTypes = {
  userProfile: PropTypes.object.isRequired,
};

export default MyProfile;
