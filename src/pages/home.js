import React, { useCallback, useContext, useRef, useState } from "react";
import styled from "styled-components";
import Card from "../components/card";
import Banner from "../components/banner/banner";
import { LoginContext } from "../components/main";
import useGetRandom from "../hooks/useGetRandom";
import { Color, Headline2 } from "../components/mixIn";
import LoaderBottom from "../components/loaderBottom";

const Container = styled.div`
  padding: 0 15px 60px;
  width: 100%;

  @media screen and (min-width: 768px) {
    padding: 10px 34px 60px;
    margin: 0 auto;
    max-width: 1320px;
  }
`;

const CardWrap = styled.div`
  @media screen and (min-width: 768px) {
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;
    margin: 0 -4px;
  }

  @media screen and (min-width: 1200px) {
    margin: 0 -7px;
  }
`;

const LoadingMsg = styled.div`
  font-size: 30px;
`;

const CategoryWrap = styled.div`
  margin-top: 40px;
  @media screen and (min-width: 768px) {
    margin-top: 60px;
  }
`;

const Subtitle = styled.h2`
  ${Headline2};
  color: ${Color.DarkGrey};
  margin-bottom: 24px;
`;

const OPTIONS = {
  thredhold: 0.5,
  rootMargin: "300px",
};

const Home = ({ history }) => {
  const { accessToken, isLogin } = useContext(LoginContext);
  const [page, setPage] = useState(0);

  const { loading, error, data, hasMore } = useGetRandom(page, accessToken);

  const observer = useRef();
  const lastElementRef = useCallback(
    (node) => {
      // node : 불러온 데이터의 마지막 요소
      if (loading) return; // loading 중에는 무한스크롤 trigger 안되도록 (이 조건 부재 시 로딩 중에 끊임없이 API 호출함)
      if (observer.current) observer.current.disconnect(); // 마지막 요소를 다시 연결하기 위해 기존 observer.current 연결 해제
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          // 관찰하고 있는 노드가 하나기 때문에 [0] , 데이터가 더 없으면 페이지를 증가시키지 않기 위해 hasmore 체크
          setPage((prevPage) => prevPage + 1); // 페이지 증가
        }
      }, OPTIONS);
      if (node) observer.current.observe(node); // 마지막 요소라면(node) 관찰 시작
    },
    [loading, hasMore]
  );

  return (
    <>
      <Container>
        <CategoryWrap>
          <Subtitle>오늘의 책상</Subtitle>
          <Banner />
        </CategoryWrap>
        <CategoryWrap>
          <Subtitle>내 책상 자랑하기</Subtitle>
          <CardWrap>
            {data &&
              data.map((item, idx) => {
                if (data.length === idx + 1) {
                  return (
                    <Card
                      cardRef={lastElementRef}
                      key={item.id}
                      item={item}
                      history={history}
                    />
                  );
                } else {
                  return <Card key={item.id} item={item} history={history} />;
                }
              })}
          </CardWrap>
        </CategoryWrap>
        <LoadingMsg>{loading && <LoaderBottom />}</LoadingMsg>
      </Container>
    </>
  );
};

export default Home;
