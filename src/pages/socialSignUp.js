import React, { useEffect, useState } from "react";
import styled from "styled-components";
import queryString from "query-string";
import DirectDomain from "../components/emailDomain/directDomain";
import SelectDomain from "../components/emailDomain/selectDomain";
import InputSns from "../components/inputSns";
import plusIcon from "../img/plus.png";
import { socialLoginApi } from "../api";
import { Cookies, useCookies } from "react-cookie";
import basicProfile from "../img/Icons/btn_profile.svg";

const Container = styled.div`
  padding: 50px 30px;
  max-width: 600px;
  margin: 0 auto;
`;

const Title = styled.h1`
  font-size: 20px;
  text-align: center;
  margin-bottom: 40px;
`;

const Form = styled.form``;

const SetImgWrap = styled.div`
  margin-top: 40px;
`;

const ImgOuterWrap = styled.div`
  position: relative;
  width: 100px;
  margin: 0 auto;
`;

const ImgWrap = styled.div`
  width: 100px;
  height: 100px;
  border-radius: 100px;
  margin: 0 auto;
  overflow: hidden;
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const ImgLabel = styled.label`
  cursor: pointer;
  margin: 15px auto 0;
  display: block;
  width: 150px;
  text-align: center;
  color: #77c4a3;
  font-size: 15px;
`;

const RemoveImgButton = styled.button`
  font-size: 15px;
  position: absolute;
  top: -4px;
  right: -10px;
`;

const InputFile = styled.input`
  overflow: hidden;
  position: absolute;
  clip: rect(0 0 0 0);
  width: 1px;
  height: 1px;
  margin: -1px;
`;

const InputWrap = styled.div`
  margin-top: 30px;
  position: relative;
`;

const EmailWrap = styled.div`
  display: flex;
`;

const AtSign = styled.span`
  color: #8e8e8e;
  display: inline-block;
  width: 26px;
  padding: 10px 6px;
`;

const Label = styled.label`
  display: block;
  font-size: 15px;
  margin-bottom: 10px;
`;

const Input = styled.input`
  width: 100%;
  padding: 10px 6px;
  border: 1px solid #bfbfbf;
  box-sizing: border-box;
  border-radius: 5px;
  margin-left: -1px;
  ${(props) => (props.flex ? "flex:1" : null)};
`;

const AddSnsButton = styled.button`
  position: absolute;
  top: -8px;
  left: 26px;
  background: url(${plusIcon}) no-repeat 9px 9px/ 12px 12px;
  padding: 15px;
`;

const SubmitButton = styled.button`
  width: 100%;
  background-color: #77c4a3;
  color: #ffffff;
  font-size: 16px;
  font-weight: 500;
  text-align: center;
  padding: 15px 0;
  border-radius: 5px;
  margin-top: 40px;
`;

const REGISTERED_DOMAINS = [
  "naver.com",
  "gmail.com",
  "hanmail.net",
  "daum.net",
  "nate.com",
];

const SocialSignUp = ({ location, history }) => {
  const [alreadyJoined, setAlreadyJoined] = useState(false);
  const [oAuthCookie, setoAuthCookie] = useCookies(["oauthToken"]);
  const [userCookie, setUserCookie] = useCookies(["userToken"]);
  const [userData, setUserData] = useState({}); //api로 받아온 소셜로그인 유저 데이터
  const [userImg, setUserImg] = useState(null);
  const [userNickname, setUserNickname] = useState("");
  const [emailId, setEmailId] = useState("");
  const [emailDomain, setEmailDomain] = useState("");
  const [userJob, setUserJob] = useState("");
  const [userLocation, setUserLocation] = useState("");
  const [userSns, setUserSns] = useState([]);

  const [isDirect, setIsDirect] = useState(() => {
    if (!REGISTERED_DOMAINS.includes(emailDomain)) {
      return true;
    } else {
      return false;
    }
  });

  const getKakaoProperties = async () => {
    const cookie = new Cookies();
    const oauthAccessToken = cookie.get("oauthToken");

    try {
      const response = await socialLoginApi.getKakaoLogin(oauthAccessToken);
      if (response.status === 200) {
        console.log("success");
        setUserData(response.data["kakao_account"]);
      }
    } catch (error) {
      console.log(error);
      if (error.response) {
        console.log(error.response);
      }
    }
  };

  const saveSocialUserData = () => {
    if (userData !== {}) {
      if (userData.profile && userData.profile.nickname) {
        setUserNickname(userData.profile.nickname);
      }
      if (userData["has_email"]) {
        const emailArr = userData.email.split("@");
        setEmailId(emailArr[0]);
        setEmailDomain(emailArr[1]);
      }
      if (userData.profile && userData.profile["profile_image_url"]) {
        setUserImg(userData.profile["profile_image_url"]);
      }
    }
  };

  useEffect(() => {
    let socialToken = queryString.parse(location.search);
    if (socialToken["oauth_user_access_token"]) {
      setoAuthCookie("oauthToken", socialToken["oauth_user_access_token"], {
        path: "/",
      });
      setAlreadyJoined(false);
      getKakaoProperties();
    } else {
      console.log("accessToken", socialToken);
      setUserCookie("userToken", socialToken["access_token"], { path: "/" });
      setAlreadyJoined(true);
    }
  }, []);

  useEffect(() => {
    saveSocialUserData();
  }, [userData]);

  const addSnsClick = (e) => {
    e.preventDefault();
    if (userSns.length >= 3) return;
    else {
      setUserSns([...userSns, ""]);
    }
  };

  const removeSnsClick = (e, index) => {
    e.preventDefault();
    if (userSns.length <= 0) return;
    else {
      setUserSns(userSns.filter((item, idx) => idx !== index));
    }
  };

  const updateSns = (e, index) => {
    setUserSns(
      userSns.map((item, idx) => (idx === index ? e.target.value : item))
    );
  };

  const handleSignUp = async () => {
    console.log("sign up");
  };

  console.log(userImg);

  return (
    <>
      <Container>
        <Title>회원가입</Title>
        <Form>
          <SetImgWrap>
            <ImgOuterWrap>
              <ImgWrap>
                <Img
                  src={
                    userImg === ""
                      ? basicProfile
                      : typeof userImg === "file"
                      ? URL.createObjectURL(userImg)
                      : userImg
                  }
                />
              </ImgWrap>
              <RemoveImgButton onClick={() => setUserImg("")}>
                ❌
              </RemoveImgButton>
            </ImgOuterWrap>
            <ImgLabel htmlFor="user_profileImg">프로필 이미지 변경</ImgLabel>
            <InputFile
              type="file"
              id="user_profileImg"
              accept=".jpg, .png"
              onChange={(e) => setUserImg(e.target.files[0])}
            />
          </SetImgWrap>
          <InputWrap>
            <Label htmlFor="user_nickname">닉네임(필수)</Label>
            <Input
              type="text"
              id="user_nickname"
              placeholder="닉네임"
              value={userNickname}
              required
              onChange={(e) => setUserNickname(e.currentTarget.value)}
            />
          </InputWrap>
          <InputWrap>
            <Label htmlFor="user_email">이메일</Label>
            <EmailWrap>
              <Input
                type="text"
                id="user_email"
                placeholder="이메일"
                value={emailId}
                flex={true}
                onChange={(e) => setEmailId(e.currentTarget.value)}
              />
              <AtSign>@</AtSign>
              {isDirect ? (
                <DirectDomain
                  isDirect={isDirect}
                  setIsDirect={setIsDirect}
                  emailDomain={emailDomain}
                  setEmailDomain={setEmailDomain}
                />
              ) : (
                <SelectDomain
                  isDirect={isDirect}
                  setIsDirect={setIsDirect}
                  setEmailDomain={setEmailDomain}
                />
              )}
            </EmailWrap>
          </InputWrap>
          <InputWrap>
            <Label htmlFor="user_job">직업</Label>
            <Input
              type="text"
              id="user_job"
              placeholder="직업"
              value={userJob}
              onChange={(e) => setUserJob(e.currentTarget.value)}
            />
          </InputWrap>
          <InputWrap>
            <Label htmlFor="user_location">지역</Label>
            <Input
              type="text"
              id="user_location"
              placeholder="지역"
              value={userLocation}
              onChange={(e) => setUserLocation(e.currentTarget.value)}
            />
          </InputWrap>
          <InputWrap>
            <Label htmlFor="user_sns">SNS</Label>
            <AddSnsButton onClick={addSnsClick} />
            {userSns.map((item, idx) => (
              <InputSns
                key={idx}
                sns={item}
                updateSns={(e) => updateSns(e, idx)}
                removeSnsClick={(e) => removeSnsClick(e, idx)}
              />
            ))}
          </InputWrap>
          <SubmitButton onClick={handleSignUp}>회원가입</SubmitButton>
        </Form>
      </Container>
    </>
  );
};

export default SocialSignUp;
