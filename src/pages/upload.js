import React, { useContext, useState } from "react";
import styled from "styled-components";
import jwt_decode from "jwt-decode";
import { contentApi } from "../api";
import Loader from "../components/loader";
import { LoginContext } from "../components/main";
import UploadedItem from "../components/uploadContents/uploadedItem";
import UploadInput from "../components/uploadContents/uploadInput";
import { useConfirm } from "../hooks/useConfirm";
import TextareaAutosize from "react-textarea-autosize";
import {
  Body1,
  Button1,
  Button2,
  Color,
  Headline2,
  Subtitle1,
  Subtitle2,
} from "../components/mixIn";

const Container = styled.div`
  position: relative;
  overflow-x: hidden;

  @media screen and (min-width: 1024px) {
    padding: 50px 0;
    width: 900px;
    margin: 50px auto;
    border: 1px solid ${Color.LightGrey2};
    border-radius: 16px;
  }
`;

const Form = styled.form``;

const Header = styled.h1`
  ${Subtitle2};
  color: ${Color.DarkGrey};
  line-height: 46px;
  position: absolute;
  top: 0;
  text-align: center;
  width: 100%;
  z-index: -1;

  @media screen and (min-width: 768px) {
    ${Headline2};
    line-height: 78px;
  }

  @media screen and (min-width: 1024px) {
    font-size: 26px;
    line-height: 30px;
    position: unset;
    top: unset;
    z-index: unset;
  }
`;

const ButtonWrap = styled.div`
  display: flex;
  justify-content: space-between;

  @media screen and (min-width: 1024px) {
    display: block;
    position: absolute;
    bottom: 10px;
    left: 0;
    right: 0;
    text-align: center;
  }
`;

const Button = styled.button`
  ${Button2};
  padding: 12px 15px;
  color: ${(props) => (props.color ? props.color : Color.DarkGrey)};

  @media screen and (min-width: 768px) {
    padding: 28px 15px;
    ${Button1};
  }

  @media screen and (min-width: 1024px) {
    padding: 0;
    width: 120px;
    height: 50px;
    border-radius: 5px;
    color: ${Color.White};
    background-color: ${(props) => (props.color ? props.color : Color.Grey1)};

    &:not(:first-child) {
      margin-left: 10px;
    }
  }
`;

const ContentWrap = styled.div`
  padding: 10px;

  @media screen and (min-width: 1024px) {
    padding: 30px;
  }
`;

const Title = styled.input`
  ${Body1};
  color: ${Color.DarkGrey};
  width: 100%;
  border: 1px solid ${Color.LightGrey2};
  border-radius: 4px;
  padding: 7px 11px;
  outline: none;

  &::placeholder {
    color: ${Color.Grey1};
  }
`;

const ItemWrap = styled.div`
  display: flex;
  flex-wrap: nowrap;
  overflow-x: scroll;
  margin-top: 16px;
`;

const TEXTAREA_STYLE = {
  resize: "none",
  width: "100%",
  padding: "10px",
  wordBreak: "break-word",
  fontSize: "16px",
  lineHeight: "24px",
  color: `${Color.DarkGrey}`,
  marginTop: "15px",
  border: "0",
  boxSizing: "border-box",
  outline: "none",
};

const Upload = ({ history }) => {
  const { accessToken } = useContext(LoginContext);

  const [title, setTitle] = useState("");
  const [images, setImages] = useState([]); // 이미지파일(file)을 담은 배열
  const [description, setDescription] = useState("");
  const [isloading, setIsLoading] = useState(false);

  const handleOut = () => history.go(-1);
  const handleNoOut = () => null;

  const confirmCancel = useConfirm(
    "이 페이지에서 나가시겠습니까?",
    handleOut,
    handleNoOut
  );

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    if (title.length === 0) {
      alert("제목을 작성해 주세요~🥺");
      setIsLoading(false);
      return;
    }
    if (images.length <= 0) {
      alert("최소 한 개 이상의 이미지를 올려야 합니다~🥺");
      setIsLoading(false);
      return;
    }
    const formData = new FormData();
    images.forEach((file) => {
      formData.append("images", file);
    });
    formData.append("description", description);
    formData.append("title", title);

    const { username } = jwt_decode(accessToken);

    try {
      const response = await contentApi.upload(formData, accessToken);

      if (response.status === 200) {
        setIsLoading(false);
        alert("WOW~ 성공적으로 게시되었습니다 🙌");
        history.push(`/users/${username}`);
      }
    } catch (error) {
      console.log(error, error.response);
      if (error.response.status === 403) {
        alert("최소 한 개 이상의 이미지를 올려야 합니다~🥺");
      }
      setIsLoading(false);
    }
  };

  return (
    <>
      {isloading && <Loader />}
      <Container>
        <Header>업로드</Header>
        <Form onSubmit={handleSubmit}>
          <ButtonWrap>
            <Button type="reset" onClick={confirmCancel}>
              취소
            </Button>
            <Button
              type="submit"
              onClick={handleSubmit}
              color={Color.DeskGreen}
            >
              게시
            </Button>
          </ButtonWrap>
          <ContentWrap>
            <Title
              required
              type="text"
              placeholder="제목을 입력하세요 :)"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              minLength="1"
              maxLength="30"
            />
            <ItemWrap>
              {images.map((item, index) => (
                <UploadedItem key={index} image={URL.createObjectURL(item)} />
              ))}
              <UploadInput images={images} setImages={setImages} />
            </ItemWrap>
            <TextareaAutosize
              placeholder="여기에 내용을 입력하세요."
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              style={TEXTAREA_STYLE}
              minRows="5"
            />
          </ContentWrap>
        </Form>
      </Container>
    </>
  );
};

export default Upload;
