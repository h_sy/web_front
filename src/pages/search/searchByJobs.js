import React, { useCallback, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import queryString from "query-string";
import { Color, Headline1, Headline2 } from "../../components/mixIn";
import { contentApi } from "../../api";
import SearchCardItem from "../../components/searchResult/searchCardItem";
import Loader from "../../components/loader";

const Container = styled.div`
  padding: 40px 30px;
  max-width: 1320px;
  width: 100%;
  margin: 0 auto;

  @media screen and (min-width: 768px) {
    padding: 40px;
  }

  @media screen and (min-width: 1024px) {
    padding: 60px 34px;
  }
`;

const KeywordWrap = styled.h2`
  ${Headline2};
  color: ${Color.DarkGrey};

  span {
    color: ${Color.DeskGreen};
    margin-right: 4px;
    text-transform: capitalize;
  }

  @media screen and (min-width: 1024px) {
    ${Headline1};
  }
`;

const Wrap = styled.div`
  position: relative;
  min-height: calc(100vh - 238px);
  padding: 30px 0;

  @media screen and (min-width: 768px) {
    padding: 60px 0;
  }
`;

const ItemWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
  margin: 0 -5px;

  @media screen and (min-width: 768px) {
    margin: 0 -10px;
  }

  @media screen and (min-width: 1024px) {
    margin: 0 -12px;
  }
`;

const LoadingMsg = styled.div``;

const OPTIONS = {
  thredhold: 0.5,
  rootMargin: "100px",
};

function SearchByJobs({ location }) {
  const { input } = queryString.parse(location.search);
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [hasMore, setHasMore] = useState(false);

  const observer = useRef();
  const lastElementRef = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPage((prevPage) => prevPage + 1);
        }
      }, OPTIONS);
      if (node) observer.current.observe(node);
    },
    [loading, hasMore]
  );

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    async function getSearchJobs(keyword, page) {
      setLoading(true);
      try {
        const response = await contentApi.searchJobs({ keyword, page });
        if (response.status === 200) {
          setData((prevData) => [...prevData, ...response.data]);
          setHasMore(response.data.length > 0);
        }
      } catch (error) {
        console.log(error.response);
      } finally {
        setLoading(false);
      }
    }

    getSearchJobs(input, page);
  }, [page]);

  return (
    <Container>
      <KeywordWrap>
        <span>{input}</span>에 대한 직업 검색 결과
      </KeywordWrap>
      <Wrap>
        <ItemWrap>
          {data.map((item, idx) => {
            if (data.length === idx + 1) {
              return (
                <SearchCardItem
                  itemRef={lastElementRef}
                  key={item.id}
                  item={item}
                />
              );
            } else {
              return <SearchCardItem key={item.id} item={item} />;
            }
          })}
        </ItemWrap>
      </Wrap>
      <LoadingMsg>{loading && <Loader />}</LoadingMsg>
    </Container>
  );
}

SearchByJobs.propTypes = {};

export default SearchByJobs;
