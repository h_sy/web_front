import React, { useEffect, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import queryString from "query-string";
import { contentApi } from "../../api";
import SearchCardItem from "../../components/searchResult/searchCardItem";
import SearchUserItem from "../../components/searchResult/searchUserItem";
import SearchNoData from "../../components/searchResult/searchNoData";
import { Color, Subtitle1, Headline2, Headline1 } from "../../components/mixIn";
import Loader from "../../components/loader";
import { Link } from "react-router-dom";
import HelmetForSEO from "../../components/helmetForSeo";

const Container = styled.div`
  padding: 40px 30px;
  max-width: 1320px;
  width: 100%;
  margin: 0 auto;

  @media screen and (min-width: 768px) {
    padding: 40px;
  }

  @media screen and (min-width: 1024px) {
    padding: 60px 34px;
  }
`;

const Wrap = styled.div`
  position: relative;
  min-height: calc(100vh - 238px);
`;

const KeywordWrap = styled.h2`
  ${Headline2};
  color: ${Color.DarkGrey};

  span {
    color: ${Color.DeskGreen};
    margin-right: 4px;
    text-transform: capitalize;
  }

  @media screen and (min-width: 1024px) {
    ${Headline1};
  }
`;

const ResultWrap = styled.div`
  position: relative;
  margin-top: 30px;

  @media screen and (min-width: 768px) {
    margin-top: 60px;
  }
`;

const ResultTitle = styled.h3`
  ${Subtitle1};
  margin-bottom: 14px;
  color: ${Color.DarkGrey};

  @media screen and (min-width: 1024px) {
    ${Headline2};
    margin-bottom: 24px;
  }

  span {
    color: ${Color.DeskGreen};
    margin-right: 4px;
    display: inline-block;
  }
`;

const ItemWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
  margin: 0 -5px;

  @media screen and (min-width: 768px) {
    margin: 0 -10px;
  }

  @media screen and (min-width: 1024px) {
    margin: 0 -12px;
  }
`;

const MoreLink = styled(Link)`
  ${Subtitle1};
  color: ${Color.DeskGreen};
  position: absolute;
  top: 0;
  right: 0;

  @media screen and (min-width: 1024px) {
    ${Headline2};
  }
`;

function Search({ location, history }) {
  const { input, page, countPerPage } = queryString.parse(location.search);
  const [userData, setUserData] = useState("");
  const [descriptionData, setDescriptionData] = useState([]);
  const [jobData, setJobData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function getSearchData() {
      setLoading(true);
      try {
        const response = await contentApi.searchAll({
          keyword: input,
          page,
          countPerPage,
        });
        if (response.status === 200) {
          const { user, descriptions, jobs } = response.data;
          setUserData(user);
          setDescriptionData(descriptions);
          setJobData(jobs);
        }
      } catch (error) {
        console.log(error.response);
      } finally {
        setLoading(false);
      }
    }

    getSearchData();
  }, [input]);

  return (
    <>
      <HelmetForSEO title={`${input}에 대한 검색 결과 | DESK Holic`} />
      <Container>
        <KeywordWrap>
          <span>{input}</span>에 대한 검색 결과
        </KeywordWrap>
        {loading ? (
          <Loader />
        ) : (
          <Wrap>
            {userData === "" &&
              (!descriptionData ||
                (descriptionData && descriptionData.length <= 0)) &&
              (!jobData || (jobData && jobData.length <= 0)) && (
                <SearchNoData />
              )}
            {userData !== "" && (
              <ResultWrap>
                <ResultTitle>유저</ResultTitle>
                <SearchUserItem user={userData} history={history} />
              </ResultWrap>
            )}
            {jobData && jobData.length > 0 && (
              <ResultWrap>
                <ResultTitle>직업</ResultTitle>
                <ItemWrap>
                  {jobData.map((item) => (
                    <SearchCardItem key={item.id} item={item} />
                  ))}
                </ItemWrap>
                {jobData && jobData.length >= 8 && (
                  <MoreLink to={`/search/job?input=${input}`}>더보기</MoreLink>
                )}
              </ResultWrap>
            )}
            {descriptionData && descriptionData.length > 0 && (
              <ResultWrap>
                <ResultTitle>컨텐츠</ResultTitle>
                <ItemWrap>
                  {descriptionData.map((item) => (
                    <SearchCardItem key={item.id} item={item} />
                  ))}
                </ItemWrap>
                {descriptionData && descriptionData.length >= 8 && (
                  <MoreLink to={`/search/content?input=${input}`}>
                    더보기
                  </MoreLink>
                )}
              </ResultWrap>
            )}
          </Wrap>
        )}
      </Container>
    </>
  );
}

Search.propTypes = {};

export default Search;
