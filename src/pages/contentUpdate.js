import React, { useContext, useState } from "react";
import styled from "styled-components";
import { useConfirm } from "../hooks/useConfirm";
import basicProfile from "../img/Icons/btn_profile.svg";
import Sliders from "../components/slider/sliders";
import TextareaAutosize from "react-textarea-autosize";
import { contentApi } from "../api";
import Loader from "../components/loader";
import SlideImgItem from "../components/slider/slideImgItem";
import { LoginContext } from "../components/main";
import {
  Button1,
  Button2,
  Caption,
  Color,
  Headline2,
  Subtitle2,
  Body1,
} from "../components/mixIn";

const Container = styled.div`
  width: 100%;

  @media screen and (min-width: 768px) {
    padding: 0 40px;
  }
  @media screen and (min-width: 1024px) {
    position: relative;
    padding: 60px 20px;
    max-width: 1320px;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
  }
`;

const Headers = styled.div`
  display: flex;
  padding: 10px;
  position: relative;
  align-items: center;

  @media screen and (min-width: 768px) {
    padding: 30px 0;
    border-bottom: 1px solid #d0d0d0;
  }
  @media screen and (min-width: 1024px) {
    position: absolute;
    top: 60px;
    right: 20px;
    width: 27%;
    height: 322px;
    border: 1px solid rgba(0, 0, 0, 0.12);
    box-sizing: border-box;
    border-radius: 16px;
    display: flex;
    flex-direction: column;
  }
`;

const SliderWrap = styled.div`
  @media screen and (min-width: 768px) {
    margin: 30px 0;
  }
  @media screen and (min-width: 1024px) {
    width: 70%;
    margin: 0;
  }
`;

const ImgWrap = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 100px;
  overflow: hidden;

  @media screen and (min-width: 768px) {
    width: 50px;
    height: 50px;
  }

  @media screen and (min-width: 1024px) {
    width: 120px;
    height: 120px;
  }
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const WriterWrap = styled.div`
  margin-left: 12px;
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media screen and (min-width: 1024px) {
    margin-left: 0;
    margin-top: 16px;
    text-align: center;
  }
`;

const Nickname = styled.div`
  ${Subtitle2};
  color: rgba(0, 0, 0, 0.6);

  @media screen and (min-width: 1024px) {
    ${Headline2};
  }
`;

const Occupation = styled.div`
  ${Caption};
  color: ${Color.Grey3};
`;

const BottomWrap = styled.div`
  padding: 15px;

  @media screen and (min-width: 768px) {
    padding: 0;
  }

  @media screen and (min-width: 1024px) {
    width: 70%;
    margin-top: 30px;
  }
`;

const Form = styled.form``;

const Title = styled.input`
  ${Body1};
  color: ${Color.DarkGrey};
  border: 1px solid ${Color.LightGrey2};
  border-radius: 4px;
  box-sizing: border-box;
  outline: none;
  padding: 10px;
  width: 100%;
`;

const TextareaStyle = {
  resize: "none",
  width: "100%",
  padding: "10px",
  wordBreak: "break-word",
  fontSize: "16px",
  lineHeight: "24px",
  color: `${Color.DarkGrey}`,
  marginTop: "15px",
  border: `1px solid ${Color.LightGrey2}`,
  borderRadius: "4px",
  boxSizing: "border-box",
  outline: "none",
};

const ButtonWrap = styled.div`
  text-align: center;
  padding: 10px 0;

  @media screen and (min-width: 1024px) {
    margin-top: 30px;
  }
`;

const Button = styled.button`
  ${Button2};
  background-color: ${Color.Grey1};
  color: ${Color.White};
  border-radius: 4px;
  padding: 10px 20px;

  &:nth-child(2) {
    background-color: ${Color.DeskGreen};
    margin-left: 5px;
  }

  @media screen and (min-width: 768px) {
    ${Button1};
    padding: 10px 30px;
  }

  @media screen and (min-width: 1024px) {
    padding: 12px 36px;
  }
`;

const ContentUpdate = ({
  location: {
    state: { item },
  },
  history,
}) => {
  const { accessToken } = useContext(LoginContext);
  const [newTitle, setNewTitle] = useState(item.title);
  const [newDescription, setNewDescription] = useState(item.description);
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      const response = await contentApi.updateContent({
        accessToken,
        id: item.id,
        title: newTitle,
        description: newDescription,
      });
      if (response.status === 200) {
        setLoading(false);
        alert("WOW~ 성공적으로 게시되었습니다 🙌");
        history.go(-1);
      }
    } catch (error) {
      console.log(error.response);
    }
  };

  const handleCancel = () => {
    history.go(-1);
  };

  const confirmCancel = useConfirm(
    "게시물 수정을 취소하시겠습니까?",
    handleCancel,
    () => console.log("취소안함")
  );

  return (
    <>
      <Container>
        {loading && <Loader />}
        <Headers>
          <ImgWrap>
            <Img
              loading="lazy"
              src={
                item.user.profileImg === ""
                  ? basicProfile
                  : item.user.profileImg
              }
            />
          </ImgWrap>
          <WriterWrap>
            <Nickname>{item.user.username}</Nickname>
            <Occupation>{item.user.job}</Occupation>
          </WriterWrap>
        </Headers>
        <SliderWrap>
          <Sliders>
            {item.images.map((img, idx) => (
              <SlideImgItem key={idx} img={img} />
            ))}
          </Sliders>
        </SliderWrap>
        <BottomWrap>
          <Form onSubmit={handleSubmit}>
            <Title
              placeholder="제목을 입력하세요 (필수)"
              value={newTitle}
              onChange={(e) => setNewTitle(e.target.value)}
            />
            <TextareaAutosize
              placeholder="내용을 입력하세요"
              value={newDescription}
              onChange={(e) => setNewDescription(e.currentTarget.value)}
              style={TextareaStyle}
            />
            <ButtonWrap>
              <Button onClick={confirmCancel}>취소</Button>
              <Button onClick={handleSubmit}>수정</Button>
            </ButtonWrap>
          </Form>
        </BottomWrap>
      </Container>
    </>
  );
};

export default ContentUpdate;
