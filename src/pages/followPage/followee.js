import React, { useEffect, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import FollowList from "../../components/follow/followList";
import FollowItem from "../../components/follow/followItem";
import { userApi } from "../../api";

const Container = styled.div`
  padding: 20px;

  @media screen and (min-width: 768px) {
    padding: 60px 100px;
  }
`;

const Title = styled.h2`
  font-size: 16px;
  font-weight: bold;
  line-height: 24px;
  margin-bottom: 14px;
  color: #333;

  @media screen and (min-width: 768px) {
    max-width: 800px;
    margin: 0 auto 18px;
    font-size: 24px;
    line-height: 36px;
  }
`;

const NoFollowings = styled.div`
  height: 50vh;
  display: flex;
  align-items: center;
  font-size: 16px;
  color: #6b6b6b;
  justify-content: center;

  @media screen and (min-width: 768px) {
    font-size: 18px;
  }
`;

function Followee({ match }) {
  const { userId } = match.params;
  const [followings, setFollowings] = useState([]);

  useEffect(() => {
    const getFollowings = async (userId) => {
      try {
        const { status, data } = await userApi.getFollowings(userId);
        if (status === 200) {
          setFollowings(data);
        }
      } catch (error) {
        console.log(error.response);
      }
    };

    if (userId !== undefined) {
      getFollowings(userId);
    }
  }, [userId]);
  return (
    <Container>
      <Title>팔로잉</Title>
      <FollowList>
        {followings && followings.length <= 0 && (
          <NoFollowings>팔로잉한 유저가 없습니다.</NoFollowings>
        )}
        {followings &&
          followings.length > 0 &&
          followings.map((following) => (
            <FollowItem key={following.id} item={following} />
          ))}
      </FollowList>
    </Container>
  );
}

Followee.propTypes = {};

export default Followee;
