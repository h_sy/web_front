import React, { useEffect, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import FollowItem from "../../components/follow/followItem";
import FollowList from "../../components/follow/followList";
import { userApi } from "../../api";

const Container = styled.div`
  padding: 20px;

  @media screen and (min-width: 768px) {
    padding: 60px 100px;
  }
`;

const Title = styled.h2`
  font-size: 16px;
  font-weight: bold;
  line-height: 24px;
  margin-bottom: 14px;
  color: #333;

  @media screen and (min-width: 768px) {
    max-width: 800px;
    margin: 0 auto 18px;
    font-size: 24px;
    line-height: 36px;
  }
`;

const NoFollowers = styled.div`
  height: 50vh;
  display: flex;
  align-items: center;
  font-size: 16px;
  color: #6b6b6b;
  justify-content: center;

  @media screen and (min-width: 768px) {
    font-size: 18px;
  }
`;

function Follower({ match }) {
  const { userId } = match.params;
  const [followers, setFollowers] = useState([]);

  useEffect(() => {
    const getFollowers = async (userId) => {
      try {
        const { status, data } = await userApi.getFollowers(userId);
        if (status === 200) {
          setFollowers(data);
        }
      } catch (error) {
        console.log(error.response);
      }
    };

    if (userId !== undefined) {
      getFollowers(userId);
    }
  }, [userId]);

  return (
    <Container>
      <Title>팔로워</Title>
      <FollowList>
        {followers && followers.length <= 0 && (
          <NoFollowers>팔로워가 없습니다.</NoFollowers>
        )}
        {followers &&
          followers.length > 0 &&
          followers.map((follower) => (
            <FollowItem key={follower.id} item={follower} />
          ))}
      </FollowList>
    </Container>
  );
}

Follower.propTypes = {};

export default Follower;
