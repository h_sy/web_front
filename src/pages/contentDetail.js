import React, { useContext, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import Sliders from "../components/slider/sliders";
import CommentList from "../components/comment/commentList";
import WritingComment from "../components/comment/writingComment";
import * as Scroll from "react-scroll";

import fillHeart from "../img/Icons/heart.svg";
import emptyHeart from "../img/Icons/empty heart.svg";
import commentIcon from "../img/Icons/comment.svg";
import basicProfile from "../img/Icons/btn_profile.svg";
import moreIcon from "../img/Icons/more.svg";
import editIcon from "../img/Icons/edit.svg";
import deleteIcon from "../img/Icons/delete_red.svg";
import { useDetectOutsideClick } from "../hooks/useDetectOutsideClick";
import { useConfirm } from "../hooks/useConfirm";
import Loader from "../components/loader";
import { contentApi } from "../api";
import NotFound from "./404";
import { Link } from "react-router-dom";
import CommentNeedsLogin from "../components/comment/commentNeedsLogin";
import { handleLikeContent, handleUnlikeContent } from "../util/httpUtil";
import SlideImgItem from "../components/slider/slideImgItem";
import { LoginContext, MyprofileContext } from "../components/main";
import FollowButton from "../components//follow/followButton";
import {
  Color,
  Body1,
  Body3,
  Subtitle1,
  Headline2,
  Subtitle2,
  Caption,
} from "../components/mixIn";
import MoreButtonModal from "../components/moreButtonModal";
import HelmetForSEO from "../components/helmetForSeo";

const Container = styled.div`
  position: relative;
  width: 100%;

  @media screen and (min-width: 768px) {
    padding: 0 40px;
  }

  @media screen and (min-width: 1024px) {
    padding: 60px 20px;
    max-width: 1320px;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
  }
`;

const Headers = styled.div`
  display: flex;
  padding: 10px;
  position: relative;
  align-items: center;
  justify-content: space-between;

  @media screen and (min-width: 768px) {
    padding: 30px 0;
    border-bottom: 1px solid ${Color.LightGrey2};
  }
  @media screen and (min-width: 1024px) {
    position: absolute;
    top: 60px;
    right: 20px;
    width: 27%;
    height: 322px;
    border: 1px solid ${Color.LightGrey2};
    box-sizing: border-box;
    border-radius: 16px;
    display: flex;
    flex-direction: column;
  }
`;

const MoreButton = styled.button`
  position: absolute;
  top: 0;
  right: 0;
  width: 50px;
  height: 40px;
`;

const SliderWrap = styled.div`
  @media screen and (min-width: 768px) {
    margin: 30px 0;
  }
  @media screen and (min-width: 1024px) {
    width: 70%;
    margin: 0;
  }
`;

const ImgWrap = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 100px;
  overflow: hidden;

  @media screen and (min-width: 768px) {
    width: 50px;
    height: 50px;
  }

  @media screen and (min-width: 1024px) {
    width: 120px;
    height: 120px;
  }
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const WriterWrap = styled.div`
  margin-left: 12px;
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media screen and (min-width: 1024px) {
    margin-left: 0;
    margin-top: 16px;
    text-align: center;
  }
`;

const Nickname = styled.div`
  ${Subtitle2};
  color: ${Color.Grey3};

  @media screen and (min-width: 1024px) {
    ${Headline2};
  }
`;

const Occupation = styled.div`
  ${Caption};
  color: ${Color.Grey3};

  @media screen and (min-width: 1024px) {
    ${Body3};
  }
`;

const BottomWrap = styled.div`
  @media screen and (min-width: 1024px) {
    width: 70%;
  }
`;

const ButtonWrap = styled.div`
  position: relative;
  margin-top: 4px;

  @media screen and (min-width: 768px) {
    margin-top: 30px;
  }
`;

const Button = styled.button`
  width: 40px;
  height: 40px;
  position: relative;
  margin-right: 6px;

  img {
    width: 24px;
    height: 24px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  @media screen and (min-width: 768px) {
    margin-right: 12px;

    img {
      width: 26px;
      height: 26px;
    }
  }
`;

const Comment = styled.img`
  width: 24px;
  height: 24px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  @media screen and (min-width: 768px) {
    width: 26px;
    height: 26px;
  }
`;

const StyleScroll = {
  display: "inline-block",
  width: "40px",
  height: "40px",
  verticalAlign: "top",
  position: "relative",
};

const Description = styled.p`
  ${Body1};
  margin-top: 8px;
  padding: 0 10px;
  word-break: break-word;
  color: ${Color.Grey3};
`;

const CountWrap = styled.div`
  display: flex;
  padding: 10px;

  @media screen and (min-width: 768px) {
    margin-top: 6px;
    padding: 10px 10px 32px;
  }
`;

const CounteTitle = styled.div`
  ${Body3};
  color: ${Color.Grey2};

  &:not(:first-child) {
    margin-left: 16px;
  }
`;

const CommentWrap = styled.div`
  @media screen and (min-width: 768px) {
    padding: 32px 0;
    border-top: 1px solid ${Color.LightGrey2};
  }
`;

const CommentTitle = styled.h3`
  display: none;
  margin: 0 10px 20px;
  ${Subtitle1};
  color: ${Color.DarkGrey};

  span {
    color: ${Color.DeskGreen};
  }

  @media screen and (min-width: 768px) {
    display: block;
  }
`;

const MenuList = styled.ul`
  padding: 8px 0;
  border-bottom: 1px solid ${Color.LightGrey2};
`;

const MenuItem = styled.li`
  width: 100%;
`;

const CancelButtonWrap = styled.div`
  padding: 8px 0;
`;

const CancelButton = styled.button`
  ${Body1};
  color: ${Color.DarkGrey};
  width: 100%;
  padding: 9px 0;

  &:hover {
    font-weight: bold;
  }
`;

const DeleteButton = styled.button`
  ${Body1};
  color: ${Color.DeskRed};
  width: 100%;
  padding: 9px 24px;
  text-align: left;
  display: flex;
  flex-direction: row;
  align-items: center;

  &:hover {
    font-weight: bold;
  }
`;

const SLink = styled(Link)`
  ${Body1};
  color: ${Color.DarkGrey};
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  padding: 9px 24px;

  &:hover {
    font-weight: bold;
  }
`;

const ProfileLink = styled(Link)`
  width: calc(100% - 80px);
  display: flex;

  @media screen and (min-width: 1024px) {
    flex-direction: column;
    align-items: center;
  }
`;

const IconWrap = styled.div`
  width: 24px;
  height: 24px;
  margin-right: 24px;

  img {
    width: 100%;
  }
`;

const FollowButtonWrap = styled.div`
  @media screen and (min-width: 1024px) {
    width: 112px;
  }
`;

const Title = styled.h2`
  ${Subtitle1}
  color: ${Color.DarkGrey};
  padding: 0 10px;
  margin-top: 10px;

  @media screen and (min-width: 1024px) {
    margin-top: 24px;
  }
`;

const ContentDetail = ({ history, match }) => {
  const { accessToken, isLogin } = useContext(LoginContext);
  const { myProfile } = useContext(MyprofileContext);

  const modalRef = useRef();
  const [isActive, setIsActive] = useDetectOutsideClick(modalRef, false);
  // const dropdownRef = useRef();
  // const [isActive, setIsActive] = useDetectOutsideClick(dropdownRef, false);
  const [comments, setComments] = useState([]);
  const [isLike, setIsLike] = useState(false);
  const [likeCount, setLikeCount] = useState(0);
  const [replyCount, setReplyCount] = useState(0);
  const [loading, setLoading] = useState(false);
  const [item, setItem] = useState(null);
  const [error, setError] = useState(null);

  const getContent = async (contentId, accessToken) => {
    setLoading(true);
    try {
      const response = await contentApi.getContent(contentId, accessToken);
      if (response.status === 200) {
        setItem(response.data);
      }
    } catch (error) {
      setError(error);
      console.log(error.response);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getContent(match.params.id, accessToken);
  }, [accessToken]);

  useEffect(() => {
    if (item && item.myLike === 1) {
      setIsLike(true);
    }

    if (item && item.contentExt) {
      setLikeCount(item.contentExt.like);
      setReplyCount(item.contentExt.replyCnt);
    }
  }, [item]);

  const showMenu = () => {
    setIsActive(!isActive);
  };

  const needLogin = useConfirm(
    "로그인이 필요한 서비스입니다. 로그인 하시겠습니까?",
    () => history.push("/login"),
    () => {
      console.log("취소");
    }
  );

  const handleLike = () => {
    if (!isLogin) {
      return needLogin();
    }
    if (isLike) {
      handleUnlikeContent(accessToken, match.params.id);
      setLikeCount(likeCount - 1);
    } else {
      handleLikeContent(accessToken, match.params.id);
      setLikeCount(likeCount + 1);
    }
    setIsLike(!isLike);
  };

  const handleDeleteContent = async () => {
    console.log("삭제");
    try {
      const response = await contentApi.deleteContent(accessToken, item.id);

      if (response.status === 200) {
        alert("삭제되었습니다😇");
        history.push("/");
      }
    } catch (error) {
      console.log(error.response);
    }
  };

  const confirmDeleteContent = useConfirm(
    "이 게시글을 삭제하시겠습니까 ?",
    handleDeleteContent,
    () => console.log("취소")
  );

  return loading ? (
    <Loader />
  ) : error ? (
    <NotFound error={error} />
  ) : (
    item !== null && (
      <>
        <HelmetForSEO
          title={`${item.user.nickname}님의 책상 | DESK Holic`}
          description={item.description}
          image={item.images[0]}
          url={`contents/${match.params.id}`}
        />
        <Container>
          <Headers>
            <ProfileLink to={`/users/${item.user.username}`}>
              <ImgWrap>
                <Img
                  src={
                    item.user.profileImg === ""
                      ? basicProfile
                      : item.user.profileImg
                  }
                />
              </ImgWrap>
              <WriterWrap>
                <Nickname>{item.user.nickname}</Nickname>
                <Occupation>{item.user.job}</Occupation>
              </WriterWrap>
            </ProfileLink>
            {myProfile.username !== item.user.username && (
              <FollowButtonWrap>
                <FollowButton userId={item.user.id} history={history} />
              </FollowButtonWrap>
            )}
          </Headers>
          <SliderWrap>
            <Sliders>
              {item.images.map((image, idx) => (
                <SlideImgItem key={idx} img={image} />
              ))}
            </Sliders>
          </SliderWrap>
          <BottomWrap>
            <ButtonWrap>
              <Button clicked={isLike} onClick={handleLike}>
                <img src={isLike ? fillHeart : emptyHeart} />
              </Button>
              <Scroll.Link
                activeClass="active"
                to="comments"
                spy={true}
                smooth={true}
                duration={500}
                style={StyleScroll}
              >
                <Comment src={commentIcon} />
              </Scroll.Link>
              {myProfile.username === item.user.username && (
                <MoreButton onClick={showMenu}>
                  <img src={moreIcon} alt="더보기" />
                </MoreButton>
              )}
            </ButtonWrap>
            <Title>{item.title}</Title>
            <Description>{item.description}</Description>
            <CountWrap>
              <CounteTitle>
                좋아요 <span>{likeCount}</span>개
              </CounteTitle>
              <CounteTitle>
                댓글 <span>{replyCount}</span>개
              </CounteTitle>
              {/* <CounteTitle>
                조회수 <span>{item.contentExt.visitCnt}</span>회
              </CounteTitle> */}
            </CountWrap>
            <CommentWrap>
              <Scroll.Element name="comments">
                <CommentTitle>
                  댓글&nbsp;<span>{replyCount}</span>
                </CommentTitle>
                {isLogin ? (
                  <WritingComment
                    contentId={item.id}
                    accessToken={accessToken}
                    comments={comments}
                    setComments={setComments}
                    setReplyCount={setReplyCount}
                    profileImg={myProfile.profileImg}
                  />
                ) : (
                  <CommentNeedsLogin />
                )}
                <CommentList
                  username={myProfile.username}
                  contentId={item.id}
                  comments={comments}
                  setComments={setComments}
                  setReplyCount={setReplyCount}
                  accessToken={accessToken}
                  profileImg={myProfile.profileImg}
                  isLogin={isLogin}
                  history={history}
                />
              </Scroll.Element>
            </CommentWrap>
          </BottomWrap>
          {isActive && (
            <MoreButtonModal isActive={isActive} modalRef={modalRef}>
              <MenuList>
                <MenuItem>
                  <SLink
                    to={{
                      pathname: `/contents/update/${match.params.id}`,
                      state: { item },
                    }}
                  >
                    <IconWrap>
                      <img src={editIcon} />
                    </IconWrap>
                    수정
                  </SLink>
                </MenuItem>
                <MenuItem>
                  <DeleteButton onClick={confirmDeleteContent}>
                    <IconWrap>
                      <img src={deleteIcon} />
                    </IconWrap>
                    삭제
                  </DeleteButton>
                </MenuItem>
              </MenuList>
              <CancelButtonWrap>
                <CancelButton onClick={showMenu}>취소</CancelButton>
              </CancelButtonWrap>
            </MoreButtonModal>
          )}
        </Container>
      </>
    )
  );
};

export default ContentDetail;
