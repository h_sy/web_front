<p align="center">
  <img src="./public/deskholic_thumbnail.svg" alt="데스크홀릭 썸네일" width="500px"/>
</p>

# 👩‍💻 DESK Holic ✏️

🏠 [Homepage](https://web.deskholic.com/)
<br />

(현재 open되어 있지 않습니다.)

<br />

## 📖 소개

<br />

직업별 업무 공간을 소개하고, 유저들과 소통 가능한 플랫폼을 개발하였습니다. 메인 컨텐츠는 "책상"입니다. 다양한 직업을 가진 사람들의 책상을 참고하여 나의 업무 공간을 만들어 나가는 데 도움이 되는 것을 목표로 하고 있습니다.

- 모든 페이지는 디바이스 화면 크기에 대응되도록 반응형으로 구현하였습니다.
- 디자인 외주를 받았고, figma로 UI디자인을 전달받아 개발했습니다.
- 배포는 npm 명령어로 가능하도록 스크립트를 작성하였고, AWS CLI를 이용하였습니다. 배포가 되면 S3에 저장이 되고, Cloudfront에서 S3를 origin으로 이용하도록 구성되어 있습니다.

<br />

## 📚 특징

1. 서로 소통할 수 있습니다.

   - 댓글 및 대댓글(답글)을 작성할 수 있습니다.
   - 유저를 팔로우 할 수 있고, 팔로워 목록 및 팔로잉 목록을 서로 확인할 수 있습니다.
   - 게시물 "좋아요👍"가 가능하며 좋아요 목록을 확인할 수 있습니다.  
     <br />

2. 내 작업공간인 책상을 자랑할 수 있습니다.
   - 책상 사진을 한 게시물에 여러장 올릴 수 있으며, 제목 및 내용을 작성하여 책상을 소개할 수 있습니다.  
     <br />
3. 검색할 수 있습니다.
   - 검색기능을 통해 나와 같은 직업을 가진 유저의 책상을 모아볼 수 있습니다.
   - 닉네임으로 유저를 검색할 수 있습니다.
   - 키워드로 내가 원하는 게시물을 모아볼 수 있습니다.  
     <br />

---

<br />

![React](https://img.shields.io/badge/React-%2341454A?logo=react&logoColor=%2361DAFB)
![Javascript](<https://img.shields.io/badge/Javscript(ES6)-%2341454A?logo=javascript&logoColor=%23F7DF1E>)
![StyledComponent](https://img.shields.io/badge/styled_components-%2341454A?logo=styledcomponents&logoColor=%23DB7093)
![Axios](https://img.shields.io/badge/Axios-%2341454A?logo=axios&logoColor=%235A29E4)
![AWS](https://img.shields.io/badge/AWS-%2341454A?logo=amazonaws&logoColor=%23F7DF1E)

<br />

<br />

## 👩‍💻 담당한 기능

- 재활용 가능한 컴포넌트 개발
- API 요청을 한 곳에서 관리할 수 있도록 모듈화하여 관리 편의성 향상
- JWT(JSON Web Token)와 쿠키를 이용하여 로그인 구현 (유효기간 하루)
- 무한스크롤 (Intersection Observer API)
- 페이지마다 각각 다른 meta 태그 설정 (React-helmet-async)
- React 내장 라이브러리인 Context API를 이용하여 상태관리
- 배너 및 컨테츠 디테일 페이지에서 이미지 슬라이더 구현 (React-slick)
- 이미지 업로드 시 크롭 기능 구현 (React-easy-crop)
- 댓글 작성 날짜로부터 얼마나 시간이 지났는지 표시 (React-moment)
- CSS-In-Js (styled-component)

<br />

## 🌏 Browser Support

| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt="IE / Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Edge | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari-ios/safari-ios_48x48.png" alt="iOS Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>iOS Safari | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/samsung-internet/samsung-internet_48x48.png" alt="Samsung" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Samsung |
| :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|                                                                                                     ✔︎                                                                                                     |                                                                                                        ✔︎                                                                                                         |                                                                                                      ✔︎                                                                                                       |                                                                                                      ✔︎                                                                                                       |                                                                                                              ✔︎                                                                                                               |                                                                                                                 ✔︎                                                                                                                  |

<br />

| Dependency        | Version |
| ----------------- | ------- |
| react             | 17.0.1  |
| react-router-dom  | 5.2.0   |
| axios             | 0.21.1  |
| styled-components | 5.2.1   |

<br />

## 📚구현 목록

1. 홈
2. 유저
   - 로그인
   - 회원가입
   - 회원 정보 수정
   - 비밀번호 변경
3. 검색
4. 컨텐츠
   - 컨텐츠 디테일
   - 컨텐츠 수정
   - 댓글, 대댓글
   - 좋아요
5. 팔로우
   - 팔로우/언팔로우 기능
   - 팔로워/팔로잉유저 목록보기
6. SNS 공유 시 OG태그를 이용한 미리보기

<br />

## 📷 스크린샷

### 홈

<div align="center"><img src="https://file.notion.so/f/f/55f2852e-9546-4a3f-a521-803d4adf31aa/b04055ea-4944-4063-a215-6695b1244529/deskholic_responsive.png?id=f7ef7f71-02e7-4526-9e22-50ddb745abdf&table=block&spaceId=55f2852e-9546-4a3f-a521-803d4adf31aa&expirationTimestamp=1700481600000&signature=sId-XJGwQ9tWmLG-QYpTye8lVZjxpOK0SFtsBVQLs_E&downloadName=deskholic_responsive.png"  
width="700px"/></div>

### 로그인

<div align="center"><img src="https://file.notion.so/f/f/55f2852e-9546-4a3f-a521-803d4adf31aa/f955a475-0081-459a-aa74-d84a9519694e/login.png?id=1cfd0d7a-1018-4216-811b-23b2e73748a1&table=block&spaceId=55f2852e-9546-4a3f-a521-803d4adf31aa&expirationTimestamp=1700481600000&signature=6Vbt75ehenmhr17gDKn_blnL0ATN8-IgGy--G1MRSqY&downloadName=login.png"  width="300px"/></div>

### 회원가입

<div align="center"><img src="https://file.notion.so/f/f/55f2852e-9546-4a3f-a521-803d4adf31aa/4c277ef9-eb70-41bf-8f77-2197671c6ef4/signup.png?id=8a9eef5f-7c29-4c31-94e8-160ddb19a77a&table=block&spaceId=55f2852e-9546-4a3f-a521-803d4adf31aa&expirationTimestamp=1700481600000&signature=y3r6-tDJEt0eiYpfdgvrlRtOsuZ5ja4QmwiSYu_xrCs&downloadName=signup.png"  width="500px"/></div>

### 업로드

<div align="center"><img src="https://file.notion.so/f/f/55f2852e-9546-4a3f-a521-803d4adf31aa/159b8395-db9c-44f2-a66b-fd407218cecf/upload.png?id=4657bc36-b657-4fbe-b3ac-c79d54548b09&table=block&spaceId=55f2852e-9546-4a3f-a521-803d4adf31aa&expirationTimestamp=1700481600000&signature=bc4eYI9OOESyjgZUUkrQ8vP05fSD9ktaj_g19IrKQ3k&downloadName=upload.png"  width="500px"/></div>

<div align="center"><img src="https://file.notion.so/f/f/55f2852e-9546-4a3f-a521-803d4adf31aa/a8d38d6e-5f9c-4aa4-ae68-73348ead622c/image_crop.png?id=14a08bbe-1f32-4ead-8352-eb0dce1759bc&table=block&spaceId=55f2852e-9546-4a3f-a521-803d4adf31aa&expirationTimestamp=1700481600000&signature=wQlMvOfUqxlxN2kB-sO10Zw127iaIvXkfs9ZNrZ8r_4&downloadName=image_crop.png"  width="500px"/></div>

### 컨텐츠 디테일 페이지

<div align="center"><img src="https://file.notion.so/f/f/55f2852e-9546-4a3f-a521-803d4adf31aa/c6b54df0-f610-4456-bb34-bee819ed0a3b/detail_1024px.png?id=fe85462f-29e6-451d-b1e0-25a3acba5215&table=block&spaceId=55f2852e-9546-4a3f-a521-803d4adf31aa&expirationTimestamp=1700481600000&signature=Hd_wh-jpod9VQjo64JH5Ccs9PryumnqcrL3QZ5YHWEM&downloadName=detail_1024px.png"  width="500px"/></div>

### 유저 페이지

<div align="center"><img src="https://file.notion.so/f/f/55f2852e-9546-4a3f-a521-803d4adf31aa/f9d2fcd7-dbba-4ce0-aef5-0877da6799a9/not_follow.png?id=06b55355-c6d2-4c75-b496-27caba7041ca&table=block&spaceId=55f2852e-9546-4a3f-a521-803d4adf31aa&expirationTimestamp=1700481600000&signature=1ndG1by-k462Xr7GSTXFxNgmhfJA4QiIe-Pqex_hxlY&downloadName=not_follow.png"  width="500px"/></div>

### 마이페이지

<div align="center"><img src="https://file.notion.so/f/f/55f2852e-9546-4a3f-a521-803d4adf31aa/26fbc082-4c15-4a06-8f1b-10d681dd40b9/mypage.png?id=8d1fefd7-d525-4c04-b393-1ef9e6cce15f&table=block&spaceId=55f2852e-9546-4a3f-a521-803d4adf31aa&expirationTimestamp=1700481600000&signature=A5DOPIil1Qzls35pQ2oVWHikNCJyd-84ADSVXy4MSWc&downloadName=mypage.png"  width="500px"/></div>

### 팔로워,팔로잉 리스트

<div align="center"><img src="https://file.notion.so/f/f/55f2852e-9546-4a3f-a521-803d4adf31aa/ca05210f-38a2-4e2a-8f4e-1e24fa9579e8/view_follow_list.png?id=8b66b2ad-5b77-4840-a551-4050ab79194b&table=block&spaceId=55f2852e-9546-4a3f-a521-803d4adf31aa&expirationTimestamp=1700481600000&signature=36X3sAhZG8Vvr5c7X7EL6TDMWKHG7S6rN1hE_fBnt54&downloadName=view_follow_list.png"  width="500px"/></div>

### 프로필 수정

<div align="center"><img src="https://file.notion.so/f/f/55f2852e-9546-4a3f-a521-803d4adf31aa/89b705b7-dd01-452d-be4e-7eb31518a3b2/profile_setting.png?id=f4333fb5-933e-4cf1-b9e0-ae28002eda03&table=block&spaceId=55f2852e-9546-4a3f-a521-803d4adf31aa&expirationTimestamp=1700481600000&signature=6UtC8YtFP0zKz8soak2BpXeA6TIM0KHCpFViEIbqmRo&downloadName=profile_setting.png"  width="500px"/></div>

### 검색

<div align="center"><img src="https://file.notion.so/f/f/55f2852e-9546-4a3f-a521-803d4adf31aa/4c1a4364-bfd6-4a49-a274-9a1ee6b2ff93/search_1024px.png?id=9fa81735-7ce1-4a54-b68f-0aaf75323982&table=block&spaceId=55f2852e-9546-4a3f-a521-803d4adf31aa&expirationTimestamp=1700481600000&signature=STK90IgWx1RZkbGE2ZCN_6Ffd2qSPJpKXRtVEim1oGY&downloadName=search_1024px.png"  width="500px"/></div>

### SNS 공유 시 og태그를 이용한 미리보기 구현

(아래 이미지에서 위가 meta 태그 적용 전, 아래가 meta 태그 적용 후)

<div align="center"><img src="https://file.notion.so/f/f/55f2852e-9546-4a3f-a521-803d4adf31aa/2891cee0-8bc6-4efa-8b09-b14c27dc913c/sns_share.jpeg?id=e26fbdf6-76ed-4d46-b226-a4bafe0fdc16&table=block&spaceId=55f2852e-9546-4a3f-a521-803d4adf31aa&expirationTimestamp=1700481600000&signature=Vl4oH28t7wpra4DUQkxZC0ECscdVdxWdwXDKZmo2uhk&downloadName=sns_share.jpeg"  width="300px"/></div>

<br />
<br />
<br />

## 🛠 TODO

<br />

- [ ] 다크모드 구현 (theme)
- [ ] 아이디/비밀번호 찾기 기능 구현
- [ ] 회원탈퇴 기능 구현
- [ ] sns 공유 시 미리보기 구현 (현재는 로고(썸네일) 및 디스크립션만 공유됨)
- [ ] SEO
- [ ] ie11 이상 지원
- [ ] 개인정보처리방침 (실제 유저를 유치하기 위해)
- [ ] 소셜 로그인 (kakao, naver, google)
